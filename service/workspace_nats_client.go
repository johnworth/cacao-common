package service

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
)

// Nats subjects
const (
	// NatsSubjectWorkspace is the prefix of the queue name for Workspace queries
	NatsSubjectWorkspace common.QueryOp = common.NatsQueryOpPrefix + "workspace"

	// WorkspaceListQueryOp is the queue name for WorkspaceList query
	WorkspaceListQueryOp common.QueryOp = NatsSubjectWorkspace + ".list"
	// WorkspaceGetQueryOp is the queue name for WorkspaceGet query
	WorkspaceGetQueryOp common.QueryOp = NatsSubjectWorkspace + ".get"

	// WorkspaceCreateRequestedEvent is the cloudevent subject for WorkspaceCreate
	WorkspaceCreateRequestedEvent common.EventType = common.EventTypePrefix + "WorkspaceCreateRequested"
	// WorkspaceDeleteRequestedEvent is the cloudevent subject for WorkspaceDelete
	WorkspaceDeleteRequestedEvent common.EventType = common.EventTypePrefix + "WorkspaceDeleteRequested"
	// WorkspaceUpdateRequestedEvent is the cloudevent subject for WorkspaceUpdate
	WorkspaceUpdateRequestedEvent common.EventType = common.EventTypePrefix + "WorkspaceUpdateRequested"

	// WorkspaceCreatedEvent is the cloudevent subject for WorkspaceCreated
	WorkspaceCreatedEvent common.EventType = common.EventTypePrefix + "WorkspaceCreated"
	// WorkspaceDeletedEvent is the cloudevent subject for WorkspaceDeleted
	WorkspaceDeletedEvent common.EventType = common.EventTypePrefix + "WorkspaceDeleted"
	// WorkspaceUpdatedEvent is the cloudevent subject for WorkspaceUpdated
	WorkspaceUpdatedEvent common.EventType = common.EventTypePrefix + "WorkspaceUpdated"

	// WorkspaceCreateFailedEvent is the cloudevent subject for WorkspaceCreateFailed
	WorkspaceCreateFailedEvent common.EventType = common.EventTypePrefix + "WorkspaceCreateFailed"
	// WorkspaceDeleteFailedEvent is the cloudevent subject for WorkspaceDeleteFailed
	WorkspaceDeleteFailedEvent common.EventType = common.EventTypePrefix + "WorkspaceDeleteFailed"
	// WorkspaceUpdateFailedEvent is the cloudevent subject for WorkspaceUpdateFailed
	WorkspaceUpdateFailedEvent common.EventType = common.EventTypePrefix + "WorkspaceUpdateFailed"
)

// Nats Client
type natsWorkspaceClient struct {
	actor      string
	emulator   string
	natsConfig messaging.NatsConfig
	stanConfig messaging.StanConfig
	ctx        context.Context
}

// NewNatsWorkspaceClient creates a client for workspace microservice that uses NATS for communication
func NewNatsWorkspaceClient(ctx context.Context, actor string, emulator string, natsConfig messaging.NatsConfig, stanConfig messaging.StanConfig) (WorkspaceClient, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "NewNatsWorkspaceClient",
	})

	logger.Trace("Creating a NatsWorkspaceClient")

	if len(actor) == 0 {
		return nil, NewCacaoInvalidParameterError("actor is empty")
	}

	if ctx == nil {
		ctx = context.Background()
	}

	natsConfigCopy := natsConfig
	if natsConfigCopy.RequestTimeout == -1 {
		natsConfigCopy.RequestTimeout = int(messaging.DefaultNatsRequestTimeout.Seconds())
	}

	stanConfigCopy := stanConfig
	if stanConfigCopy.EventsTimeout == -1 {
		stanConfigCopy.EventsTimeout = int(messaging.DefaultStanEventsTimeout.Seconds())
	}

	client := natsWorkspaceClient{
		actor:      actor,
		emulator:   emulator,
		natsConfig: natsConfigCopy,
		stanConfig: stanConfigCopy,
		ctx:        ctx,
	}

	return &client, nil
}

func waitWorkspaceResponse(c chan WorkspaceModel, timeout time.Duration) (bool, WorkspaceModel) {
	select {
	case res := <-c:
		return true, res
	case <-time.After(timeout):
		return false, WorkspaceModel{}
	}
}

// Get returns the workspace with the given workspace ID
func (client *natsWorkspaceClient) Get(workspaceID common.ID) (Workspace, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsWorkspaceClient.Get",
	})

	if len(workspaceID) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid Workspace ID")
	}

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := WorkspaceModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID: workspaceID,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, WorkspaceGetQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a get workspace event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var workspaceModel WorkspaceModel
	err = json.Unmarshal(responseBytes, &workspaceModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to workspace object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := workspaceModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &workspaceModel, nil
}

// List returns all workspaces for the user
func (client *natsWorkspaceClient) List() ([]Workspace, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsWorkspaceClient.List",
	})

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := Session{
		SessionActor:    client.actor,
		SessionEmulator: client.emulator,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, WorkspaceListQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a list workspaces event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var workspaceListModel WorkspaceListModel
	err = json.Unmarshal(responseBytes, &workspaceListModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to workspace list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := workspaceListModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return workspaceListModel.GetWorkspaces(), nil
}

// Create creates a new workspace
func (client *natsWorkspaceClient) Create(workspace Workspace) (common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsWorkspaceClient.Create",
	})

	if len(workspace.GetName()) == 0 {
		return common.ID(""), NewCacaoInvalidParameterError("invalid Workspace Name")
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return common.ID(""), NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return common.ID(""), NewCacaoCommunicationError(errorMessage)
	}

	workspaceID := workspace.GetID()
	if !workspaceID.Validate() {
		workspaceID = NewWorkspaceID()
	}

	request := WorkspaceModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID:                workspaceID,
		Owner:             client.actor,
		Name:              workspace.GetName(),
		Description:       workspace.GetDescription(),
		DefaultProviderID: workspace.GetDefaultProviderID(),
	}

	eventsSubscribe := []common.EventType{
		WorkspaceCreatedEvent,
		WorkspaceCreateFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan WorkspaceModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == WorkspaceCreatedEvent || ev == WorkspaceCreateFailedEvent {
			var workspaceModel WorkspaceModel
			err := json.Unmarshal(ce.Data(), &workspaceModel)
			if err != nil {
				errorMessage := "unable to unmarshal a workspace create response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- WorkspaceModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- workspaceModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- WorkspaceModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return common.ID(""), NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(WorkspaceCreateRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish a create workspace event"
		logger.WithError(err).Error(errorMessage)
		return common.ID(""), NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitWorkspaceResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return workspaceID, nil
		}

		logger.Error(serviceError)
		return common.ID(""), serviceError
	}

	// timed out
	logger.Error("unable to create a workspace - workspace service is not responding")
	return common.ID(""), NewCacaoTimeoutError("unable to create a workspace - workspace service is not responding")
}

// Update updates the workspace with the workspace ID
func (client *natsWorkspaceClient) Update(workspace Workspace) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsWorkspaceClient.Update",
	})

	if !workspace.GetID().Validate() {
		return NewCacaoInvalidParameterError("invalid Workspace ID")
	}

	if len(workspace.GetName()) == 0 {
		return NewCacaoInvalidParameterError("invalid Workspace Name")
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	request := WorkspaceModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID:                workspace.GetID(),
		Name:              workspace.GetName(),
		Description:       workspace.GetDescription(),
		DefaultProviderID: workspace.GetDefaultProviderID(),
	}

	eventsSubscribe := []common.EventType{
		WorkspaceUpdatedEvent,
		WorkspaceUpdateFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan WorkspaceModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == WorkspaceUpdatedEvent || ev == WorkspaceUpdateFailedEvent {
			var workspaceModel WorkspaceModel
			err := json.Unmarshal(ce.Data(), &workspaceModel)
			if err != nil {
				errorMessage := "unable to unmarshal a workspace update response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- WorkspaceModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- workspaceModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- WorkspaceModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(WorkspaceUpdateRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish a update workspace event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitWorkspaceResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return nil
		}

		logger.Error(serviceError)
		return serviceError
	}

	// timed out
	logger.Error("unable to update a workspace - workspace service is not responding")
	return NewCacaoTimeoutError("unable to update a workspace - workspace service is not responding")
}

// UpdateFields updates the selected fields of the workspace with the workspace ID
func (client *natsWorkspaceClient) UpdateFields(workspace Workspace, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsWorkspaceClient.UpdateFields",
	})

	if !workspace.GetID().Validate() {
		return NewCacaoInvalidParameterError("invalid Workspace ID")
	}

	if len(updateFieldNames) == 0 {
		return nil
	}

	for _, updateFieldName := range updateFieldNames {
		switch updateFieldName {
		case "name":
			if len(workspace.GetName()) == 0 {
				return NewCacaoInvalidParameterError("invalid Workspace Name")
			}
		default:
			//pass
		}
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	request := WorkspaceModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID:                workspace.GetID(),
		Name:              workspace.GetName(),
		Description:       workspace.GetDescription(),
		DefaultProviderID: workspace.GetDefaultProviderID(),
		UpdateFieldNames:  updateFieldNames,
	}

	eventsSubscribe := []common.EventType{
		WorkspaceUpdatedEvent,
		WorkspaceUpdateFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan WorkspaceModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == WorkspaceUpdatedEvent || ev == WorkspaceUpdateFailedEvent {
			var workspaceModel WorkspaceModel
			err := json.Unmarshal(ce.Data(), &workspaceModel)
			if err != nil {
				errorMessage := "unable to unmarshal a workspace update response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- WorkspaceModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- workspaceModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- WorkspaceModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(WorkspaceUpdateRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish a update workspace event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitWorkspaceResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return nil
		}

		logger.Error(serviceError)
		return serviceError
	}

	// timed out
	logger.Error("unable to update a workspace - workspace service is not responding")
	return NewCacaoTimeoutError("unable to update a workspace - workspace service is not responding")
}

// Delete deletes the workspace with the workspace ID
func (client *natsWorkspaceClient) Delete(workspaceID common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsWorkspaceClient.Delete",
	})

	if !workspaceID.Validate() {
		return NewCacaoInvalidParameterError("invalid Workspace ID")
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	request := WorkspaceModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID: workspaceID,
	}

	eventsSubscribe := []common.EventType{
		WorkspaceDeletedEvent,
		WorkspaceDeleteFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan WorkspaceModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == WorkspaceDeletedEvent || ev == WorkspaceDeleteFailedEvent {
			var workspaceModel WorkspaceModel
			err := json.Unmarshal(ce.Data(), &workspaceModel)
			if err != nil {
				errorMessage := "unable to unmarshal a workspace delete response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- WorkspaceModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- workspaceModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- WorkspaceModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(WorkspaceDeleteRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish a delete workspace event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitWorkspaceResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return nil
		}

		logger.Error(serviceError)
		return serviceError
	}

	// timed out
	logger.Error("unable to delete a workspace - workspace service is not responding")
	return NewCacaoTimeoutError("unable to delete a workspace - workspace service is not responding")
}
