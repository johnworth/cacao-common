package service

import (
	"context"
	"encoding/json"
	"errors"
	"sync"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/rs/xid"

	log "github.com/sirupsen/logrus"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"

	nats "github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
)

// Nats Subjects root, get, list and other channels
const (
	NatsSubjectUsers     string = common.NatsQueryOpPrefix + "users"
	NatsSubjectUsersGet  string = NatsSubjectUsers + ".get"
	NatsSubjectUsersList string = NatsSubjectUsers + ".list"

	EventUserAddRequested    common.EventType = "org.cyverse.events.UserAddRequested"
	EventUserUpdateRequested common.EventType = "org.cyverse.events.UserUpdateRequested"
	EventUserDeleteRequested common.EventType = "org.cyverse.events.UserDeleteRequested"

	EventUserAdded   common.EventType = "org.cyverse.events.UserAdded"
	EventUserUpdated common.EventType = "org.cyverse.events.UserUpdated"
	EventUserDeleted common.EventType = "org.cyverse.events.UserDeleted"

	EventUserAddError    common.EventType = "org.cyverse.events.UserAddError"
	EventUserUpdateError common.EventType = "org.cyverse.events.UserUpdateError"
	EventUserDeleteError common.EventType = "org.cyverse.events.UserDeleteError"
)

// // These are User options
// type NatsUserOptions func(u *natsuserclient)

// func ForcePreferencesOption(user *natsuserclient) { // this option will force the loading and saving of preferences with a User object or User objects in the list
// 	user.User.ForceLoadPreferences = true
// }

// // These are UserList options
// type NatsUserListOptions func(u *NatsUserList)

// func ForcePreferencesListOption(ulist *NatsUserList) { // this option will force the loading and saving of preferences with a User object or User objects in the list
// 	ulist.UserList.ForceLoadPreferences = true
// }

// This object should never be serialized/marshalled
type natsUserClient struct {
	actor          string
	emulator       string
	clientID       string
	clusterID      string // used for stan
	natsUrl        string
	maxReconnect   int
	reconnectWait  time.Duration
	requestTimeout time.Duration
	eventsChannel  string
	eventsTimeout  time.Duration
	ctx            context.Context
	eventSrc       messaging.EventObservable
}

func NewNatsUserClient(ctx context.Context, actor string, emulator string, natsConfig messaging.NatsConfig, stanConfig messaging.StanConfig) (UserClient, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "NewNatsUserClient",
	})
	logger.Trace("NewNatsUserClient() called")

	if actor == "" {
		return nil, errors.New(UserActorNotSetError)
	}

	if ctx == nil {
		ctx = context.Background()
	}
	new_user_svc := natsUserClient{
		actor:         actor,
		emulator:      emulator,
		natsUrl:       natsConfig.URL,
		clientID:      natsConfig.ClientID,
		clusterID:     stanConfig.ClusterID,
		eventsChannel: common.EventsSubject,
		ctx:           ctx}

	logger.WithFields(log.Fields{
		"natsUrl":       new_user_svc.natsUrl,
		"clientID":      new_user_svc.clientID,
		"clusterID":     new_user_svc.clusterID,
		"eventsChannel": new_user_svc.eventsChannel,
	}).Trace()

	// for _, option := range options { // call the options
	// 	option(&new_user_svc)
	// }

	if natsConfig.MaxReconnects <= 0 {
		new_user_svc.maxReconnect = messaging.DefaultNatsMaxReconnect
	} else {
		new_user_svc.maxReconnect = natsConfig.MaxReconnects
	}
	if natsConfig.ReconnectWait <= 0 {
		new_user_svc.reconnectWait = messaging.DefaultNatsReconnectWait
	} else {
		new_user_svc.reconnectWait = time.Duration(natsConfig.ReconnectWait) * time.Second
	}
	if natsConfig.RequestTimeout <= 0 {
		new_user_svc.requestTimeout = messaging.DefaultNatsRequestTimeout
	} else {
		new_user_svc.requestTimeout = time.Duration(natsConfig.RequestTimeout) * time.Second
	}
	if stanConfig.EventsTimeout <= 0 {
		new_user_svc.eventsTimeout = messaging.DefaultStanEventsTimeout
	} else {
		new_user_svc.eventsTimeout = time.Duration(stanConfig.EventsTimeout) * time.Second
	}

	// create EventSource with a different ClientID
	eventSrcNatsConf := natsConfig
	eventSrcNatsConf.ClientID += "-" + xid.New().String()
	new_user_svc.eventSrc = messaging.NewEventSource(eventSrcNatsConf, stanConfig)

	return &new_user_svc, nil
}

func (usersvc *natsUserClient) Add(user User) error {
	log.Trace("natsuserclient.Add() started")
	return usersvc.publishRequestEvent(user, EventUserAddRequested, true)
}

func (usersvc *natsUserClient) Update(user User) error {
	log.Trace("natsuserclient.Update() started")
	return usersvc.publishRequestEvent(user, EventUserUpdateRequested, true)
}

// save will do the logic for saving a user whether it's a create or update, but also allow for async operation
func (usersvc *natsUserClient) publishRequestEvent(user User, eventOpType common.EventType, waitForCompletion bool) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsUserClient.publishRequestEvent",
	})
	var u *UserModel = user.(*UserModel)
	logger.WithFields(log.Fields{
		"username":          u.Username,
		"eventOpType":       string(eventOpType),
		"waitForCompletion": waitForCompletion,
	}).Trace("publishRequestEvent() called")

	if u.Username == "" {
		return errors.New(UserUsernameNotSetError)
	}

	// let's set session actor and emulator from svc object
	u.SessionActor = usersvc.actor
	u.SessionEmulator = usersvc.actor

	logger.Trace("stan connect for request")
	sc, err := stan.Connect(usersvc.clusterID, usersvc.clientID, stan.NatsURL(usersvc.natsUrl))
	if err != nil {
		return err
	}
	defer sc.Close()

	logger.Trace("creating cloud event")
	transactionID := messaging.NewTransactionID()
	ce, err := messaging.CreateCloudEventWithTransactionID(user, string(eventOpType), usersvc.clientID, transactionID)
	if err != nil {
		return err
	}
	payload, err := json.Marshal(ce)
	if err != nil {
		return err
	}

	if waitForCompletion {

		// note, since we need to handle a custom cancellation process, rather than
		// a simple cancel or timeout through context, let's just setup a context with
		// cancel enabled
		ctx, cancel := context.WithCancel(usersvc.ctx)
		resultChan := make(chan cloudevents.Event)
		defer close(resultChan)

		// TODO remove once
		// Since ListenOnce==false, need to do sync here to ensure handler is called ONLY once.
		var once sync.Once
		handler := func(ev common.EventType, ce cloudevents.Event) {
			// TODO after User MS support Transaction ID, just push to channel, no need to perform username check here
			var eventUser UserModel
			err := json.Unmarshal(ce.Data(), &eventUser)
			if err != nil {
				return
			}
			if eventUser.Username == u.Username {
				once.Do(func() {
					logger.WithField("username", eventUser.Username).Trace("pushed to channel")
					resultChan <- ce
				})
			} else {
				logger.WithField("username", eventUser.Username).Trace("different username as expected")
			}
		}

		var listenerID messaging.ListenerID

		// listens to different event type depends on event operation
		switch eventOpType {
		case EventUserAddRequested:
			// TODO register listener with transactionID, currently User MS does not support Transaction ID
			// Note: using transaction ID wildcard means there cannot be 2 concurrent listener of the same event type,
			// e.g. no 2 concurrent UserAdd.
			// This is due to the way messaging.EventSource is implemented.
			// TODO use ListenOnce after User MS support TransactionID
			listenerID, err = usersvc.eventSrc.AddListenerMultiEventType([]common.EventType{EventUserAdded, EventUserAddError},
				"", messaging.Listener{Callback: handler, ListenOnce: false})
		case EventUserUpdateRequested:
			// TODO register listener with transactionID
			// TODO use ListenOnce after User MS support TransactionID
			listenerID, err = usersvc.eventSrc.AddListenerMultiEventType([]common.EventType{EventUserUpdated, EventUserUpdateError},
				"", messaging.Listener{Callback: handler, ListenOnce: false})
		case EventUserDeleteRequested:
			// TODO register listener with transactionID
			// TODO use ListenOnce after User MS support TransactionID
			listenerID, err = usersvc.eventSrc.AddListenerMultiEventType([]common.EventType{EventUserDeleted, EventUserDeleteError},
				"", messaging.Listener{Callback: handler, ListenOnce: false})
		default:
			panic("unknown op") // unknown op must not be passed
		}
		if err != nil {
			cancel()
			return err
		}

		logger.Trace("sync publishing the event " + eventOpType)
		err = sc.Publish(usersvc.eventsChannel, payload)
		if err != nil {
			cancel()
			usersvc.eventSrc.RemoveListenerByID(listenerID)
			return err
		}

		var result cloudevents.Event
		select {
		case result = <-resultChan:
		case <-time.After(usersvc.eventsTimeout):
			cancel()
			return errors.New(GeneralEventOpTimeoutError)
		case <-ctx.Done():
			cancel()
			return errors.New(GeneralEventOpCanceledError)
		}
		defer cancel()

		// TODO this is no longer needed if ListenOnce==true
		// Since ListenOnce==false, listener always needs to be remove.
		usersvc.eventSrc.RemoveListenerByID(listenerID)

		eventUser := UserModel{}
		err = json.Unmarshal(result.Data(), &eventUser)
		if err != nil {
			return err
		}
		// at this point, we have the user
		if eventUser.ErrorType != "" {
			logger.Trace("result received, errorResult = " + eventUser.ErrorType)
			return errors.New(eventUser.ErrorType)
		} else {
			logger.Trace("result received, errorResult = nil")
		}
		if eventUser.Username == u.Username { // u was declared at the beginning of publishRequestEvent
			logger.WithField("username", u.Username).Trace("found the user")
			return nil
		} else {
			errorMessage := "miss-matched username in response"
			logger.WithFields(log.Fields{
				"expect": u.Username,
				"got":    eventUser.Username,
			}).Error(errorMessage)
			return errors.New(errorMessage) // FIXME create an error type
		}
	} else {
		logger.Trace("async publishing the event " + eventOpType)
		err = sc.Publish(usersvc.eventsChannel, payload)
		if err != nil {
			return err
		}
	}

	return nil
}

// NewNatsUser creates a new NatsUser struct based on the nats connection; developers will still need
// to Load() after creating a new NatsUser
// actor is the username of the person making requests
// source is a string representing the source/service that is invoking the request (used for cloudevent)
// ctx can be nil, if one isn't available
func (usersvc *natsUserClient) Get(username string) (User, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsUserClient.Get",
	})
	logger.Trace("Get() started")

	if username == "" {
		return nil, errors.New(UserUsernameNotSetError)
	}

	logger.WithField("natsUrl", usersvc.natsUrl).Trace("connecting to nats")
	nc, err := nats.Connect(usersvc.natsUrl, nats.MaxReconnects(usersvc.maxReconnect), nats.ReconnectWait(usersvc.reconnectWait))
	if err != nil {
		return nil, err
	}
	defer nc.Close()

	logger.Trace("creating cloud event")
	user := UserModel{Session: Session{SessionActor: usersvc.actor, SessionEmulator: usersvc.emulator},
		Username: username}
	ce, err := messaging.CreateCloudEvent(user, NatsSubjectUsersGet, usersvc.clientID)
	if err != nil {
		return nil, err
	}
	payload, err := json.Marshal(ce)
	if err != nil {
		return nil, err
	}

	// retrieve the loaded user from the nats ether
	logger.Trace("sending request with context to " + NatsSubjectUsersGet)
	newctx, cancel := context.WithTimeout(usersvc.ctx, usersvc.requestTimeout)
	msg, err := nc.RequestWithContext(newctx, NatsSubjectUsersGet, payload)
	cancel() // no longer need the context, so cancel at this point
	if err != nil {
		return nil, err
	}

	// convert the message to a cloud event
	logger.Trace("received event, converting and unmarshalling")
	ce, err = messaging.ConvertNats(msg)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(ce.Data(), &user)
	if err != nil {
		return nil, err
	}

	// final check for error
	if user.ErrorType != "" {
		logger.WithField("err", user.ErrorType).Trace("error was received")
		err = errors.New(user.ErrorType)
	}

	return &user, err
}

func (usersvc *natsUserClient) Delete(user User) error {
	log.Trace("natsUserClient.Delete() started")
	return usersvc.publishRequestEvent(user, EventUserDeleteRequested, true)
}

func (usersvc *natsUserClient) Search(filter UserListFilter) (UserList, error) {
	log.Trace("natsUserClient.Search() started")
	ul := &UserListModel{Session: Session{SessionActor: usersvc.actor, SessionEmulator: usersvc.emulator},
		Filter: filter}

	err := usersvc.load(ul)
	if err != nil {
		ul = nil
	}
	return ul, err
}

func (usersvc *natsUserClient) SearchNext(ul UserList) error {
	log.Trace("natsUserClient.SearchNext() started")

	var ulm *UserListModel = ul.(*UserListModel)

	if ulm.NextStart < 0 {
		return errors.New(UserListLoadBoundaryError)
	}

	// just set the filter index to the new starting point
	ulm.Filter.Start = ulm.NextStart

	return usersvc.load(ulm)
}

func (usersvc natsUserClient) load(ul *UserListModel) error {
	log.Trace("natsUserClient.load() started")

	// TODO: replace when implemented
	return errors.New(GeneralMethodNotYetImplementedError)
	/*
		if ul.Filter == (UserListFilter{}) { // compare with an uninitialized UserListFilter
			return errors.New(UserListFilterNotSetError)
		}
		if ul.Filter.Value == "" {
			log.Warning("NatsUserList.Load: " + UserListFilterValueEmptyWarning)
		}
		if ul.Filter.MaxItems < 1 {
			return errors.New(UserListFilterInvalidMaxItemsError)
		}
		if ul.Filter.Start < 0 {
			return errors.New(UserListFilterInvalidStartIndexError)
		}
		if ul.Filter.SortBy == 0 { // default sort
			ul.Filter.SortBy = AscendingSort
		}

		nc, err := nats.Connect(usersvc.natsUrl, nats.MaxReconnects(messaging.DefaultNatsMaxReconnect), nats.ReconnectWait(messaging.DefaultNatsReconnectWait))
		if err != nil {
			return err
		}
		defer nc.Close()

		ce, err := messaging.CreateCloudEvent(ul, NatsSubjectUsersList, usersvc.clientID)
		if err != nil {
			return err
		}
		payload, err := json.Marshal(ce)

		if err != nil {
			return err
		}

		// retrieve the loaded user from the nats ether
		newctx, cancel := context.WithTimeout(usersvc.ctx, usersvc.requestTimeout)
		msg, err := nc.RequestWithContext(newctx, NatsSubjectUsersList, payload)
		cancel() // no longer need the context, so cancel at this point
		if err != nil {
			return err
		}

		// convert the message to a cloud event
		ce, err = messaging.ConvertNats(msg)
		if err != nil {
			return err
		}

		err = json.Unmarshal(ce.Data(), &ul)
		if err != nil {
			return err
		}

		return nil
	*/
}
