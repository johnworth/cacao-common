package service

import "gitlab.com/cyverse/cacao-common/common"

// AWMProvider is the ID of cloud provider, used by Argo Workflow Mediator(AWM)
type AWMProvider string

// WorkflowCreateRequestedEvent is the type name of WorkflowCreate request
const WorkflowCreateRequestedEvent common.EventType = common.EventTypePrefix + "WorkflowCreateRequested"

// WorkflowCreate is an event that request the creation of a workflow.
type WorkflowCreate struct {
	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider AWMProvider `json:"provider"`

	// username of the user, null if not user specific
	Username *string `json:"username"`

	// filename of the workflow definition
	WorkflowFilename string `json:"workflow_filename"`

	// workflow data, parameters and other metadata used to create the workflow
	WfDat map[string]interface{} `json:"workflow_data"`
}

// WorkflowCreatedEvent is the cloudevent name of WorkflowCreated defined below
const WorkflowCreatedEvent common.EventType = common.EventTypePrefix + "WorkflowCreated"

// WorkflowCreated is an event emitted when a Workflow is created.
type WorkflowCreated struct {
	// ID of the request event (WorkflowCreate) that caused this event
	RequestID common.EventID `json:"req_id"`

	// The provider of which the workflow is created in
	Provider AWMProvider `json:"provider"`

	// Name of the created workflow
	WorkflowName string `json:"workflow_name"`
}

// WorkflowCreateFailedEvent is the event name of WorkflowCreateFailed
const WorkflowCreateFailedEvent common.EventType = common.EventTypePrefix + "WorkflowCreateFailed"

// WorkflowCreateFailed is an event emitted when failure occurred during
// workflow creation.
// This event is emitted in response to a WorkflowCreate event.
type WorkflowCreateFailed struct {
	// ID of the request event (WorkflowCreate) that caused this event
	RequestID common.EventID `json:"req_id"`

	// The provider that the workflow is attempted to be created in
	Provider AWMProvider `json:"provider"`

	// Type of the error occurred that cause the creation to fail
	Error string `json:"error"`

	// Extra error message
	Msg string `json:"msg"`
}

// WorkflowTerminateRequestedEvent is the type name of WorkflowTerminate request
const WorkflowTerminateRequestedEvent common.EventType = common.EventTypePrefix + "WorkflowTerminateRequested"

// WorkflowTerminate is an event that request the terminatation of a workflow.
type WorkflowTerminate struct {
	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider AWMProvider `json:"provider"`

	// name of the workflow
	WorkflowName string `json:"workflow_name"`
}

// WorkflowTerminatedEvent is the name of the WorkflowTerminated
const WorkflowTerminatedEvent common.EventType = common.EventTypePrefix + "WorkflowTerminated"

// WorkflowTerminated is an event emitted when a workflow is terminated via a WorkflowTerminateCmd event.
type WorkflowTerminated struct {
	// ID of the request event (WorkflowTerminate) that caused this event
	RequestID common.EventID `json:"req_id"`

	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider AWMProvider `json:"provider"`

	// name of the workflow
	WorkflowName string `json:"workflow_name"`

	// status of the workflow
	WorkflowStatus string `json:"workflow_status"`
}

// WorkflowTerminateFailedEvent is the name of the WorkflowTerminateFailed
const WorkflowTerminateFailedEvent common.EventType = common.EventTypePrefix + "WorkflowTerminateFailed"

// WorkflowTerminateFailed is an event emitted when fail to terminate a workflow.
type WorkflowTerminateFailed struct {
	// ID of the request event (WorkflowTerminate) that caused this event
	RequestID common.EventID `json:"req_id"`

	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider AWMProvider `json:"provider"`

	// name of the workflow
	WorkflowName string `json:"workflow_name"`

	// Type of the error occurred that cause the termination to fail
	Error string `json:"error"`

	// Extra error message
	Msg string `json:"msg"`
}

// WorkflowResubmitRequestedEvent is the type name of WorkflowResubmit request
const WorkflowResubmitRequestedEvent common.EventType = common.EventTypePrefix + "WorkflowResubmitRequested"

// WorkflowResubmit is an event that request the resubmission of a workflow.
type WorkflowResubmit struct {
	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider AWMProvider `json:"provider"`

	// name of the workflow
	WorkflowName string `json:"workflow_name"`
}

// WorkflowResubmittedEvent is the name of the WorkflowResubmitted event
const WorkflowResubmittedEvent common.EventType = common.EventTypePrefix + "WorkflowResubmitted"

// WorkflowResubmitted is an event that is emitted when workflow resubmission succeeded.
type WorkflowResubmitted struct {
	// ID of the request event (WorkflowResubmit) that caused this event
	RequestID common.EventID `json:"req_id"`

	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider AWMProvider `json:"provider"`

	// name of the workflow that is requested to be resubmitted
	WorkflowName string `json:"workflow_name"`

	// name of the newly created workflow as the result of the resubmission
	NewWorkflowName string `json:"new_workflow_name"`
}

// WorkflowResubmitFailedEvent is the name of the WorkflowResubmitFailed event
const WorkflowResubmitFailedEvent common.EventType = common.EventTypePrefix + "WorkflowResubmitFailed"

// WorkflowResubmitFailed is an event that is emitted when workflow resubmission failed.
type WorkflowResubmitFailed struct {

	// ID of the request event (WorkflowTerminate) that caused this event
	RequestID common.EventID `json:"req_id"`

	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider AWMProvider `json:"provider"`

	// name of the workflow that is requested to be resubmitted
	WorkflowName string `json:"workflow_name"`

	// Type of the error occurred that cause the resubmission to fail
	Error string `json:"error"`

	// Extra error message
	Msg string `json:"msg"`
}

// TerraformWorkflowData is the parameter to required to create a Terraform workflow.
// Corrsponds to WfDat in WorkflowCreate
type TerraformWorkflowData struct {
	// ID of the Terraform template to be applied. This will be used to name
	// the Terraform workspace used when appling the template, and keyed the
	// Terraform State in the Terraform backend
	TemplateID string `mapstructure:"template_id"`
	// name of the terraform module to execute as child module
	TfModuleName string `mapstructure:"module_name"`
	Username     string `mapstructure:"username"`
	// ID of the cloud credential to use
	CloudCredID string                 `mapstructure:"cloud_cred_id"`
	AnsibleVars map[string]interface{} `mapstructure:"ansible_vars"`
	// Encrypted cloud credential encoded in base64
	CloudCredentialBase64 string `mapstructure:"cloud_cred"`
}

// OpenStackCredential is credential for OpenStack. This include both
// username/password and Application Credential.
type OpenStackCredential struct {
	IdentityAPIVersion string `json:"OS_IDENTITY_API_VERSION" mapstructure:"OS_IDENTITY_API_VERSION"`
	RegionName         string `json:"OS_REGION_NAME" mapstructure:"OS_REGION_NAME"`
	Interface          string `json:"OS_INTERFACE" mapstructure:"OS_INTERFACE"`
	AuthURL            string `json:"OS_AUTH_URL" mapstructure:"OS_AUTH_URL"`
	ProjectDomainID    string `json:"OS_PROJECT_DOMAIN_ID" mapstructure:"OS_PROJECT_DOMAIN_ID"`
	ProjectDomainName  string `json:"OS_PROJECT_DOMAIN_NAME" mapstructure:"OS_PROJECT_DOMAIN_NAME"`
	ProjectID          string `json:"OS_PROJECT_ID" mapstructure:"OS_PROJECT_ID"`
	ProjectName        string `json:"OS_PROJECT_NAME" mapstructure:"OS_PROJECT_NAME"`
	UserDomainName     string `json:"OS_USER_DOMAIN_NAME" mapstructure:"OS_USER_DOMAIN_NAME"`

	Username string `json:"OS_USERNAME" mapstructure:"OS_USERNAME"`
	Password string `json:"OS_PASSWORD" mapstructure:"OS_PASSWORD"`

	AuthType      string `json:"OS_AUTH_TYPE" mapstructure:"OS_AUTH_TYPE"`
	AppCredID     string `json:"OS_APPLICATION_CREDENTIAL_ID" mapstructure:"OS_APPLICATION_CREDENTIAL_ID"`
	AppCredName   string `json:"OS_APPLICATION_CREDENTIAL_NAME" mapstructure:"OS_APPLICATION_CREDENTIAL_NAME"`
	AppCredSecret string `json:"OS_APPLICATION_CREDENTIAL_SECRET" mapstructure:"OS_APPLICATION_CREDENTIAL_SECRET"`
}
