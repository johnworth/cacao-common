package service

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
)

// ReplyListStream contains fields needed for streaming a list of objects in multiple replies
type ReplyListStream struct {
	// total number of replies, at least 1
	TotalReplies int `json:"total_reply"`
	// index of the reply, start at 0
	ReplyIndex int `json:"reply_index"`
}

// RequestPagination contains fields needed to request pagination in the list result
type RequestPagination struct {
	// offset that a page begins on
	Offset int `json:"offset"`
	// limit of page size, -1 for unlimited page size
	PageSizeLimit int `json:"limit"`
}

const (
	// DeploymentGetQueryType ...
	DeploymentGetQueryType common.QueryOp = "cyverse.deployment.get"
	// DeploymentListQueryType ...
	DeploymentListQueryType common.QueryOp = "cyverse.deployment.list"
	// DeploymentGetRunQueryType ...
	DeploymentGetRunQueryType common.QueryOp = "cyverse.deployment.getRun"
	// DeploymentListRunQueryType ...
	DeploymentListRunQueryType common.QueryOp = "cyverse.deployment.listRun"
	// DeploymentGetRawStateQueryType ...
	DeploymentGetRawStateQueryType common.QueryOp = "cyverse.deployment.getRawState"
	// DeploymentGetLogsQueryType ...
	DeploymentGetLogsQueryType common.QueryOp = "cyverse.deployment.getLogs"
)

// DeploymentGetQuery is a query to fetch a single deployment
type DeploymentGetQuery struct {
	Session `json:",inline"`
	ID      common.ID `json:"id"`
}

// DeploymentGetReply is reply to Get query
type DeploymentGetReply struct {
	Session      `json:",inline"`
	Deployment   Deployment `json:"deployment"`
	DeploymentID common.ID  `json:"id"`
}

// ToCloudEvent converts to cloudevent
func (r DeploymentGetReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(DeploymentGetQueryType), source)
}

// DeploymentListQuery is a query to fetch a list of deployments
type DeploymentListQuery struct {
	Session       `json:",inline"`
	Filters       DeploymentFilter `json:"filters"`
	SortBy        SortByField      `json:"sort_by"`
	SortDirection SortDirection    `json:"sort_dir"`
	Offset        int              `json:"offset"`
	// no limit if -1.
	Limit int `json:"limit"`
}

// DeploymentListReply is reply to List query
type DeploymentListReply struct {
	Session     `json:",inline"`
	Offset      int          `json:"offset"`
	Count       int          `json:"count"`
	Deployments []Deployment `json:"deployments"`

	ReplyListStream `json:",inline"`
}

// ToCloudEvent converts to cloudevent
func (r DeploymentListReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(DeploymentListQueryType), source)
}

// DeploymentGetRunQuery is a query to fetch a specific instance of deployment run
type DeploymentGetRunQuery struct {
	Session    `json:",inline"`
	Deployment common.ID `json:"deployment"`
	Run        common.ID `json:"run"`
}

// DeploymentGetRunReply is reply yo GetRun query
type DeploymentGetRunReply struct {
	Session    `json:",inline"`
	Deployment common.ID     `json:"deployment"`
	Run        DeploymentRun `json:"run"`
}

// ToCloudEvent converts to cloudevent
func (r DeploymentGetRunReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(DeploymentGetRunQueryType), source)
}

// DeploymentListRunQuery is a query that fetch a list of deployment run for a specific deployment
type DeploymentListRunQuery struct {
	Session           `json:",inline"`
	Deployment        common.ID `json:"deployment"`
	RequestPagination `json:",inline"`
}

// DeploymentListRunReply is reply to ListRun query
type DeploymentListRunReply struct {
	Session `json:",inline"`
	Offset  int             `json:"offset"`
	Count   int             `json:"count"`
	Runs    []DeploymentRun `json:"runs"`

	ReplyListStream `json:",inline"`
}

// ToCloudEvent converts to cloudevent
func (r DeploymentListRunReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(DeploymentListRunQueryType), source)
}

// DeploymentGetRawStateQuery is a query to fetch the most recent raw state of a deployment run
type DeploymentGetRawStateQuery struct {
	Session    `json:",inline"`
	Deployment common.ID `json:"deployment"`
	Run        common.ID `json:"run"`
}

// DeploymentGetRawStateReply is reply for GetRawState query
type DeploymentGetRawStateReply struct {
	Session  `json:",inline"`
	RawState []byte `json:"raw_state"`
}

// ToCloudEvent converts to cloudevent
func (r DeploymentGetRawStateReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(DeploymentGetRawStateQueryType), source)
}

// DeploymentGetLogsQuery is a query to fetch the current logs of a deployment run
type DeploymentGetLogsQuery struct {
	Session    `json:",inline"`
	Deployment common.ID `json:"deployment"`
	Run        common.ID `json:"run"`
}

// DeploymentGetLogsReply is reply for GetLogs query
type DeploymentGetLogsReply struct {
	Session    `json:",inline"`
	Deployment common.ID `json:"deployment"`
	Logs       string    `json:"logs"`
}

// ToCloudEvent converts to cloudevent
func (r DeploymentGetLogsReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(DeploymentGetLogsQueryType), source)
}

// DeploymentCreationRequested ...
const DeploymentCreationRequested common.EventType = common.EventTypePrefix + "DeploymentCreationRequested"

// DeploymentCreationRequest is an event that request the creation of a deployment
type DeploymentCreationRequest struct {
	Session     `json:",inline"`
	CreateParam DeploymentCreateParam `json:",inline"`
}

// DeploymentCreated ...
const DeploymentCreated common.EventType = common.EventTypePrefix + "DeploymentCreated"

// DeploymentCreateFailed ...
const DeploymentCreateFailed common.EventType = common.EventTypePrefix + "DeploymentCreateFailed"

// DeploymentCreationResult is an event that contains the result to the creation request
type DeploymentCreationResult struct {
	Session `json:",inline"`
	ID      common.ID `json:"id"`
}

// DeploymentStateRefreshRequested ...
const DeploymentStateRefreshRequested common.EventType = common.EventTypePrefix + "DeploymentStateRefreshRequested"

// DeploymentStateRefresh is an event to request a state refresh on a deployment
type DeploymentStateRefresh struct {
	Session `json:",inline"`
	ID      common.ID `json:"id"`
}

// DeploymentStateRefreshed ...
const DeploymentStateRefreshed common.EventType = common.EventTypePrefix + "DeploymentRefreshStateRequested"

// DeploymentStateRefreshFailed ...
const DeploymentStateRefreshFailed common.EventType = common.EventTypePrefix + "DeploymentStateRefreshFailed"

// DeploymentStateRefreshResult an event that contains result to the state refresh request
type DeploymentStateRefreshResult struct {
	Session `json:",inline"`
	ID      common.ID           `json:"id"`
	State   DeploymentStateView `json:"state"`
}

// DeploymentParameterUpdateRequested ...
const DeploymentParameterUpdateRequested common.EventType = common.EventTypePrefix + "DeploymentParameterUpdateRequested"

// DeploymentParameterUpdateRequest is an event that request updating paramters of a deployment
type DeploymentParameterUpdateRequest struct {
	Session     `json:",inline"`
	Deployment  common.ID                 `json:"id"`
	ParamValues DeploymentParameterValues `json:"param_values"`
}

// DeploymentParameterUpdated ...
const DeploymentParameterUpdated common.EventType = common.EventTypePrefix + "DeploymentParameterUpdated"

// DeploymentParameterUpdateFailed ...
const DeploymentParameterUpdateFailed common.EventType = common.EventTypePrefix + "DeploymentParameterUpdateFailed"

// DeploymentParameterUpdateResult is an event that contains the result to the update parameter request
type DeploymentParameterUpdateResult struct {
	Session `json:",inline"`
	ID      common.ID
}

// DeploymentDeletionRequested ...
const DeploymentDeletionRequested common.EventType = common.EventTypePrefix + "DeploymentDeletionRequested"

// DeploymentDeletionRequest is an event that request the deletion of a deployment
type DeploymentDeletionRequest struct {
	Session `json:",inline"`
	// deployment ID
	ID common.ID `json:"id"`
}

// DeploymentDeletionStarted indicate that the deletion has started but not yet finished. This event is only emitted if
// the deletion involves clean up cloud resources.
// Note: deletion could still fail after this.
const DeploymentDeletionStarted common.EventType = common.EventTypePrefix + "DeploymentDeletionStarted"

// DeploymentDeleted ...
const DeploymentDeleted common.EventType = common.EventTypePrefix + "DeploymentDeleted"

// DeploymentDeleteFailed ...
const DeploymentDeleteFailed common.EventType = common.EventTypePrefix + "DeploymentDeleteFailed"

// DeploymentDeletionResult is an event that contains the result to the deletion request
type DeploymentDeletionResult struct {
	Session `json:",inline"`
	// deployment ID
	ID common.ID `json:"id"`
}

// DeploymentCreateRunRequested ...
const DeploymentCreateRunRequested common.EventType = common.EventTypePrefix + "DeploymentCreateRunRequested"

// DeploymentCreateRunRequest is an event that request the creation of a deployment run
type DeploymentCreateRunRequest struct {
	Session     `json:",inline"`
	CreateParam DeploymentRunCreateParam `json:",inline"`
}

// DeploymentRunCreated ...
const DeploymentRunCreated common.EventType = common.EventTypePrefix + "DeploymentRunCreated"

// DeploymentRunCreateFailed ...
const DeploymentRunCreateFailed common.EventType = common.EventTypePrefix + "DeploymentRunCreateFailed"

// DeploymentCreateRunResult is an event that contains the result to the create run request
type DeploymentCreateRunResult struct {
	Session `json:",inline"`
	Run     DeploymentRun `json:"run"`
}

// DeploymentStartMonitoringRequested ...
const DeploymentStartMonitoringRequested common.EventType = common.EventTypePrefix + "DeploymentStartMonitoringRequested"

// DeploymentStartMonitoring is an event that request the specified deployment to be monitored
type DeploymentStartMonitoring struct {
	Session `json:",inline"`
	// deployment ID
	ID common.ID `json:"id"`
}

// DeploymentMonitoringStarted ...
const DeploymentMonitoringStarted common.EventType = common.EventTypePrefix + "DeploymentMonitoringStarted"

// DeploymentMonitoringStartFailed ...
const DeploymentMonitoringStartFailed common.EventType = common.EventTypePrefix + "DeploymentMonitoringStartFailed"

// DeploymentStartMonitoringResult is an event that contains the result to start monitoring request
type DeploymentStartMonitoringResult struct {
	Session `json:",inline"`
	// deployment ID
	ID common.ID `json:"id"`
}

// DeploymentStopMonitoringRequested ...
const DeploymentStopMonitoringRequested common.EventType = common.EventTypePrefix + "DeploymentStopMonitoringRequested"

// DeploymentStopMonitoring is an event that request to stop monitoring the specified deployment
type DeploymentStopMonitoring struct {
	Session `json:",inline"`
	// deployment ID
	ID common.ID `json:"id"`
}

// DeploymentMonitoringStopped ...
const DeploymentMonitoringStopped common.EventType = common.EventTypePrefix + "DeploymentMonitoringStopped"

// DeploymentMonitoringStopFailed ...
const DeploymentMonitoringStopFailed common.EventType = common.EventTypePrefix + "DeploymentMonitoringStopFailed"

// DeploymentStopMonitoringResult is an event that contains the result to stop monitoring request
type DeploymentStopMonitoringResult struct {
	Session `json:",inline"`
	// deployment ID
	ID common.ID `json:"id"`
}
