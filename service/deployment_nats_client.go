package service

import (
	"context"
	"encoding/json"
	"time"

	"github.com/rs/xid"

	cloudevents "github.com/cloudevents/sdk-go/v2"

	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
)

// natsDeploymentClient implements DeploymentClient
type natsDeploymentClient struct {
	actor    string
	emulator string
	natsConf messaging.NatsConfig
	stanConf messaging.StanConfig
	// NATS Streaming channel name that all Events are published and received from
	eventChannel string
	eventSrc     messaging.EventObservable
	eventTimeout time.Duration

	// TODO make this a parameter to all exposed methods, e.g. deploymentClient.Get(ctx, .....)
	ctx context.Context
}

// NewDeploymentClient creates a new DeploymentClient
func NewDeploymentClient(ctx context.Context, actor, emulator string,
	natsConf messaging.NatsConfig, stanConf messaging.StanConfig) DeploymentClient {
	var eventTimeout time.Duration
	if time.Duration(stanConf.EventsTimeout) > 0 {
		eventTimeout = time.Second * time.Duration(stanConf.EventsTimeout)
	} else {
		eventTimeout = messaging.DefaultStanEventsTimeout
	}

	return natsDeploymentClient{
		actor:        actor,
		emulator:     emulator,
		natsConf:     natsConf,
		stanConf:     stanConf,
		eventChannel: common.EventsSubject,
		eventSrc:     messaging.NewEventSource(natsConf, stanConf),
		eventTimeout: eventTimeout,
		ctx:          ctx,
	}
}

func (client natsDeploymentClient) natsConnect() (*nats.Conn, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.natsConnect",
	})

	nc, err := nats.Connect(client.natsConf.URL,
		nats.MaxReconnects(client.natsConf.MaxReconnects),
		nats.ReconnectWait(time.Duration(client.natsConf.ReconnectWait)*time.Second))
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}
	return nc, nil
}

func (client natsDeploymentClient) stanConnect() (stan.Conn, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.stanConnect",
	})

	// this connection is only used for publishing
	sc, err := stan.Connect(client.stanConf.ClusterID,
		client.natsConf.ClientID+"-"+xid.New().String(),
		stan.NatsURL(client.natsConf.URL))
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}
	return sc, nil
}

// clientID is client ID is used as the prefix for generating unique name for source in cloudevent
// and client ID for STAN connection.
func (client natsDeploymentClient) clientID() string {
	return client.natsConf.ClientID
}

// Get fetch a single deployment by its ID
func (client natsDeploymentClient) Get(deploymentID common.ID) (*Deployment, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.Get",
	})
	query := DeploymentGetQuery{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID: deploymentID}

	ce, err := client.natsRequest(client.ctx, query, DeploymentGetQueryType)
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return nil, err
	}

	var reply DeploymentGetReply
	err = json.Unmarshal(ce.Data(), &reply)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to DeploymentGetReply"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := reply.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return &reply.Deployment, nil
}

// Search returns a list of deployment, with optional filters and sort.
func (client natsDeploymentClient) Search(option DeploymentListOption) (DeploymentList, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.Search",
	})
	query := DeploymentListQuery{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Filters:       option.Filter,
		SortBy:        option.SortBy,
		SortDirection: option.SortDirection,
		Offset:        option.Offset,
		Limit:         option.PageSize,
	}
	replyCountExtractor := func(ce *cloudevents.Event) int {
		var reply DeploymentListReply
		err := json.Unmarshal(ce.Data(), &reply)
		if err != nil {
			return 0
		}
		return reply.TotalReplies
	}

	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*5)
	eventList, err := client.natsRequestMultiRelies(ctx, query, DeploymentListQueryType, replyCountExtractor)
	cancel()
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return nil, err
	}

	deploymentList := make([]Deployment, 0)
	for _, ce := range eventList {
		var reply DeploymentListReply
		err = json.Unmarshal(ce.Data(), &reply)
		if err != nil {
			errorMessage := "unable to unmarshal response JSON to DeploymentListReply"
			logger.WithError(err).Error(errorMessage)
			return nil, NewCacaoMarshalError(errorMessage)
		}

		serviceError := reply.GetServiceError()
		if serviceError != nil {
			logger.Error(serviceError)
			return nil, serviceError
		}

		deploymentList = append(deploymentList, reply.Deployments...)
	}

	return deploymentListModel{
		Option:      option,
		Deployments: deploymentList,
	}, nil
}

// SearchNext returns a list of deployment, with optional filters and sort.
func (client natsDeploymentClient) SearchNext(list DeploymentList) (DeploymentList, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.SearchNext",
	})
	query := DeploymentListQuery{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Filters:       list.GetOptions().Filter,
		SortBy:        list.GetOptions().SortBy,
		SortDirection: list.GetOptions().SortDirection,
		Offset:        list.GetNextStart(),
		Limit:         list.GetOptions().PageSize,
	}
	replyCountExtractor := func(ce *cloudevents.Event) int {
		var reply DeploymentListReply
		err := json.Unmarshal(ce.Data(), &reply)
		if err != nil {
			return 0
		}
		return reply.TotalReplies
	}

	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*5)
	eventList, err := client.natsRequestMultiRelies(ctx, query, DeploymentListQueryType, replyCountExtractor)
	cancel()
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return nil, err
	}

	deploymentList := make([]Deployment, 0)
	for _, ce := range eventList {
		var reply DeploymentListReply
		err = json.Unmarshal(ce.Data(), &reply)
		if err != nil {
			errorMessage := "unable to unmarshal response JSON to DeploymentListReply"
			logger.WithError(err).Error(errorMessage)
			return nil, NewCacaoMarshalError(errorMessage)
		}

		serviceError := reply.GetServiceError()
		if serviceError != nil {
			logger.Error(serviceError)
			return nil, serviceError
		}

		deploymentList = append(deploymentList, reply.Deployments...)
	}

	return deploymentListModel{
		Option:      list.GetOptions(),
		Deployments: deploymentList,
	}, nil
}

// GetRun returns a specific deployment run.
func (client natsDeploymentClient) GetRun(deployment, run common.ID) (*DeploymentRun, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.GetRun",
	})
	query := DeploymentGetRunQuery{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Deployment: deployment,
		Run:        run,
	}

	ce, err := client.natsRequest(client.ctx, query, DeploymentGetRunQueryType)
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return nil, err
	}

	var reply DeploymentGetRunReply
	err = json.Unmarshal(ce.Data(), &reply)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to DeploymentGetRunReply"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := reply.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return &reply.Run, nil
}

// SearchRun returns a list of deployment run.
func (client natsDeploymentClient) SearchRun(option DeploymentRunListOption) (DeploymentRunList, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.SearchRun",
	})
	query := DeploymentListRunQuery{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Deployment: option.Deployment,
		RequestPagination: RequestPagination{
			Offset:        option.Offset,
			PageSizeLimit: option.PageSize,
		},
	}
	repliesCountExtractor := func(ce *cloudevents.Event) int {
		var reply DeploymentListRunReply
		err := json.Unmarshal(ce.Data(), &reply)
		if err != nil {
			return 0
		}
		return reply.TotalReplies
	}

	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*5)
	eventList, err := client.natsRequestMultiRelies(ctx, query, DeploymentListRunQueryType, repliesCountExtractor)
	cancel()
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return nil, err
	}

	runList := make([]DeploymentRun, 0)
	for _, ce := range eventList {
		var reply DeploymentListRunReply
		err = json.Unmarshal(ce.Data(), &reply)
		if err != nil {
			errorMessage := "unable to unmarshal response JSON to DeploymentListRunReply"
			logger.WithError(err).Error(errorMessage)
			return nil, NewCacaoMarshalError(errorMessage)
		}

		serviceError := reply.GetServiceError()
		if serviceError != nil {
			logger.Error(serviceError)
			return nil, serviceError
		}

		runList = append(runList, reply.Runs...)
	}

	return deploymentRunListModel{
		Runs:   runList,
		Option: option,
	}, nil
}

// SearchRunNext continues the Search with pagination.
func (client natsDeploymentClient) SearchRunNext(list DeploymentRunList) (DeploymentRunList, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.SearchRunNext",
	})
	query := DeploymentListRunQuery{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Deployment: list.GetOption().Deployment,
		RequestPagination: RequestPagination{
			Offset:        list.GetNextStart(),
			PageSizeLimit: list.GetSize(),
		},
	}
	repliesCountExtractor := func(ce *cloudevents.Event) int {
		var reply DeploymentListRunReply
		err := json.Unmarshal(ce.Data(), &reply)
		if err != nil {
			return 0
		}
		return reply.TotalReplies
	}

	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*5)
	eventList, err := client.natsRequestMultiRelies(ctx, query, DeploymentListRunQueryType, repliesCountExtractor)
	cancel()
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return nil, err
	}

	runList := make([]DeploymentRun, 0)
	for _, ce := range eventList {
		var reply DeploymentListRunReply
		err = json.Unmarshal(ce.Data(), &reply)
		if err != nil {
			errorMessage := "unable to unmarshal response JSON to DeploymentListRunReply"
			logger.WithError(err).Error(errorMessage)
			return nil, NewCacaoMarshalError(errorMessage)
		}

		serviceError := reply.GetServiceError()
		if serviceError != nil {
			logger.Error(serviceError)
			return nil, serviceError
		}

		runList = append(runList, reply.Runs...)
	}

	return deploymentRunListModel{
		Runs:   runList,
		Option: list.GetOption(),
	}, nil
}

// GetRawState fetch the raw state of a deployment run.
func (client natsDeploymentClient) GetRawState(deployment, run common.ID) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.GetRawState",
	})
	query := DeploymentGetRawStateQuery{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Deployment: deployment,
		Run:        run,
	}

	ce, err := client.natsRequest(client.ctx, query, DeploymentGetRawStateQueryType)
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return nil, err
	}

	var reply DeploymentGetRawStateReply
	err = json.Unmarshal(ce.Data(), &reply)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to DeploymentGetRawStateReply"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := reply.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return reply.RawState, nil
}

// GetLogs fetch the logs of a deployment run.
func (client natsDeploymentClient) GetLogs(deployment, run common.ID) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.GetLogs",
	})
	query := DeploymentGetLogsQuery{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Deployment: deployment,
		Run:        run,
	}

	ce, err := client.natsRequest(client.ctx, query, DeploymentGetLogsQueryType)
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return "", err
	}

	var reply DeploymentGetLogsReply
	err = json.Unmarshal(ce.Data(), &reply)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to DeploymentGetLogsReply"
		logger.WithError(err).Error(errorMessage)
		return "", NewCacaoMarshalError(errorMessage)
	}

	serviceError := reply.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return "", serviceError
	}

	return reply.Logs, nil
}

// CreateAsync sends request to create a deployment
func (client natsDeploymentClient) CreateAsync(tid common.TransactionID, option DeploymentCreateParam) error {
	event := DeploymentCreationRequest{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		CreateParam: option,
	}
	return client.stanPublish(event, DeploymentCreationRequested, tid)
}

// Create creates a deployment. Deployment ID is returned on success.
func (client natsDeploymentClient) Create(tid common.TransactionID, option DeploymentCreateParam) (common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.Create",
	})
	if tid == "" {
		tid = messaging.NewTransactionID()
	}

	var resultChan = make(chan cloudevents.Event)
	defer close(resultChan)
	listenerID, err := client.listenForResponse([]common.EventType{DeploymentCreated, DeploymentCreateFailed},
		tid, resultChan)
	if err != nil {
		logger.WithError(err).Error("unable to listen for a response")
		return common.ID(""), err
	}

	err = client.CreateAsync(tid, option)
	if err != nil {
		logger.WithError(err).Error("unable to create a deployment")
		client.eventSrc.RemoveListenerByID(listenerID)
		return common.ID(""), err
	}

	var resultEvent DeploymentCreationResult
	eventType, err := client.waitForResponse(listenerID, resultChan, &resultEvent)
	if err != nil {
		logger.WithError(err).Error("unable to receive a response")
		return common.ID(""), err
	}

	serviceError := resultEvent.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return common.ID(""), serviceError
	}

	if eventType == DeploymentCreated {
		logger.WithFields(log.Fields{"ID": resultEvent.ID}).Trace("received result")
		return resultEvent.ID, nil
	}

	return common.ID(""), NewCacaoGeneralError("failed to create a deployment")
}

// RefreshStateAsync sends request to refresh state
func (client natsDeploymentClient) RefreshStateAsync(tid common.TransactionID, deployment common.ID) error {
	event := DeploymentStateRefresh{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID: deployment,
	}
	return client.stanPublish(event, DeploymentStateRefreshRequested, tid)
}

// RefreshState actively refresh the state of a deployment's latest run. The refreshed state is returned on success.
func (client natsDeploymentClient) RefreshState(tid common.TransactionID, deployment common.ID) (DeploymentStateView, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.RefreshState",
	})
	if tid == "" {
		tid = messaging.NewTransactionID()
	}

	resultChan := make(chan cloudevents.Event)

	listenerEventTypes := []common.EventType{DeploymentStateRefreshed, DeploymentStateRefreshFailed}
	listenerID, err := client.listenForResponse(listenerEventTypes, tid, resultChan)
	if err != nil {
		logger.WithError(err).Error("unable to listen for a response")
		return DeploymentStateView{}, err
	}

	err = client.RefreshStateAsync(tid, deployment)
	if err != nil {
		logger.WithError(err).Error("unable to refresh deployment state")
		client.eventSrc.RemoveListenerByID(listenerID)
		return DeploymentStateView{}, err
	}

	var resultEvent DeploymentStateRefreshResult
	eventType, err := client.waitForResponse(listenerID, resultChan, &resultEvent)
	if err != nil {
		logger.WithError(err).Error("unable to receive a response")
		return DeploymentStateView{}, err
	}

	serviceError := resultEvent.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return DeploymentStateView{}, serviceError
	}

	if eventType == DeploymentStateRefreshed {
		logger.WithFields(log.Fields{"ID": resultEvent.ID}).Trace("received result")
		return resultEvent.State, nil
	}

	return DeploymentStateView{}, NewCacaoGeneralError("failed to refresh deployment state")
}

// UpdateParametersAsync sends request to update parameters of a deployment.
// Only the parameters that needs to be updated are required.
func (client natsDeploymentClient) UpdateParametersAsync(tid common.TransactionID, deployment common.ID, param DeploymentParameterValues) error {
	event := DeploymentParameterUpdateRequest{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Deployment:  deployment,
		ParamValues: param,
	}
	return client.stanPublish(event, DeploymentParameterUpdateRequested, tid)
}

// UpdateParameters updates the parameter and create a deployment run. The newly created deployment run ID
// is returned on success.
func (client natsDeploymentClient) UpdateParameters(tid common.TransactionID, deployment common.ID, param DeploymentParameterValues) (common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.UpdateParameters",
	})
	if tid == "" {
		tid = messaging.NewTransactionID()
	}

	var resultChan = make(chan cloudevents.Event)
	listenerEventTypes := []common.EventType{DeploymentParameterUpdated, DeploymentParameterUpdateFailed}
	listenerID, err := client.listenForResponse(listenerEventTypes, tid, resultChan)
	if err != nil {
		logger.WithError(err).Error("unable to listen for a response")
		return "", err
	}

	err = client.UpdateParametersAsync(tid, deployment, param)
	if err != nil {
		logger.WithError(err).Error("unable to update deployment parameters")
		client.eventSrc.RemoveListenerByID(listenerID)
		return "", err
	}

	var resultEvent DeploymentParameterUpdateResult
	receivedEventType, err := client.waitForResponse(listenerID, resultChan, &resultEvent)
	if err != nil {
		logger.WithError(err).Error("unable to receive a response")
		return "", err
	}

	serviceError := resultEvent.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return "", serviceError
	}

	if receivedEventType == DeploymentParameterUpdated {
		logger.WithFields(log.Fields{"ID": resultEvent.ID}).Trace("received result")
		return resultEvent.ID, nil
	}

	return "", NewCacaoGeneralError("failed to update deployment parameters")
}

// DeleteAsync sends request to delete a deployment
func (client natsDeploymentClient) DeleteAsync(tid common.TransactionID, deployment common.ID) error {
	event := DeploymentDeletionRequest{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID: deployment}
	return client.stanPublish(event, DeploymentDeletionRequested, tid)
}

// Delete deletes a deployment.
func (client natsDeploymentClient) Delete(tid common.TransactionID, deployment common.ID) (deleted bool, err error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.Delete",
	})
	if tid == "" {
		tid = messaging.NewTransactionID()
	}

	var resultChan = make(chan cloudevents.Event)
	listenerEventTypes := []common.EventType{DeploymentDeleted, DeploymentDeletionStarted, DeploymentDeleteFailed}
	listenerID, err := client.listenForResponse(listenerEventTypes, tid, resultChan)
	if err != nil {
		logger.WithError(err).Error("unable to listen for a response")
		return false, err
	}

	err = client.DeleteAsync(tid, deployment)
	if err != nil {
		logger.WithError(err).Error("unable to delete a deployment")
		client.eventSrc.RemoveListenerByID(listenerID)
		return false, err
	}

	var resultEvent DeploymentDeletionResult
	receivedEventType, err := client.waitForResponse(listenerID, resultChan, &resultEvent)
	if err != nil {
		logger.WithError(err).Error("unable to receive a response")
		return false, err
	}

	serviceError := resultEvent.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return false, serviceError
	}

	if receivedEventType == DeploymentDeleted {
		logger.WithFields(log.Fields{"ID": resultEvent.ID}).Trace("received result, deleted")
		return true, nil
	} else if receivedEventType == DeploymentDeletionStarted {
		// This event means that the deletion request is received and the deletion process has started.
		// But the deployment may not have been deleted yet.
		// There will be another DeploymentDeleted/DeploymentDeletionFailed event after this to indicate the final
		// result of the deletion, but the final event that not be tracked by this function.
		logger.WithFields(log.Fields{
			"ID": resultEvent.ID,
		}).Trace("received result, deletion started")
		return false, nil
	}
	return false, NewCacaoGeneralError("failed to delete a deployment")
}

// CreateRunAsync sends request to create a deployment run
func (client natsDeploymentClient) CreateRunAsync(tid common.TransactionID, param DeploymentRunCreateParam) error {
	event := DeploymentCreateRunRequest{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		CreateParam: param,
	}
	return client.stanPublish(event, DeploymentCreateRunRequested, tid)
}

// CreateRun creates a new deployment run. Deployment Run is returned on success.
func (client natsDeploymentClient) CreateRun(tid common.TransactionID, param DeploymentRunCreateParam) (*DeploymentRun, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.CreateRun",
	})
	if tid == "" {
		tid = messaging.NewTransactionID()
	}

	var resultChan = make(chan cloudevents.Event)
	listenerEventTypes := []common.EventType{DeploymentRunCreated, DeploymentRunCreateFailed}
	listenerID, err := client.listenForResponse(listenerEventTypes, tid, resultChan)
	if err != nil {
		logger.WithError(err).Error("unable to listen for a response")
		return nil, err
	}

	err = client.CreateRunAsync(tid, param)
	if err != nil {
		logger.WithError(err).Error("unable to create a deployment run")
		client.eventSrc.RemoveListenerByID(listenerID)
		return nil, err
	}

	var resultEvent DeploymentCreateRunResult
	receivedEventType, err := client.waitForResponse(listenerID, resultChan, &resultEvent)
	if err != nil {
		logger.WithError(err).Error("unable to receive a response")
		return nil, err
	}

	serviceError := resultEvent.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	if receivedEventType == DeploymentRunCreated {
		logger.WithFields(log.Fields{"runID": resultEvent.Run.ID}).Trace("received result")
		return &resultEvent.Run, nil
	}

	return nil, NewCacaoGeneralError("failed to create a deployment run")
}

// StartMonitoringAsync sends request to start monitor a deployment
// TODO monitor deployment or monitor deployment run?
func (client natsDeploymentClient) StartMonitoringAsync(tid common.TransactionID, deployment common.ID) error {
	event := DeploymentStartMonitoring{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID: deployment}
	return client.stanPublish(event, DeploymentStartMonitoringRequested, tid)
}

// StartMonitoring starts monitoring a deployment.
func (client natsDeploymentClient) StartMonitoring(tid common.TransactionID, deployment common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.StartMonitoring",
	})
	if tid == "" {
		tid = messaging.NewTransactionID()
	}

	var resultChan = make(chan cloudevents.Event)
	listenerEventTypes := []common.EventType{DeploymentMonitoringStarted, DeploymentMonitoringStartFailed}
	listenerID, err := client.listenForResponse(listenerEventTypes, tid, resultChan)
	if err != nil {
		logger.WithError(err).Error("unable to listen for a response")
		return err
	}

	err = client.StartMonitoringAsync(tid, deployment)
	if err != nil {
		logger.WithError(err).Error("unable to start deployment monitoring")
		client.eventSrc.RemoveListenerByID(listenerID)
		return err
	}

	var resultEvent DeploymentStartMonitoringResult
	receivedEventType, err := client.waitForResponse(listenerID, resultChan, &resultEvent)
	if err != nil {
		logger.WithError(err).Error("unable to receive a response")
		return err
	}

	serviceError := resultEvent.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return serviceError
	}

	if receivedEventType == DeploymentMonitoringStarted {
		logger.WithFields(log.Fields{"ID": resultEvent.ID}).Trace("received result")
		return nil
	}

	return NewCacaoGeneralError("failed to start deployment monitoring")
}

// StopMonitoringAsync sends request to stop monitor a deployment
// TODO monitor deployment or monitor deployment run?
func (client natsDeploymentClient) StopMonitoringAsync(tid common.TransactionID, deployment common.ID) error {
	event := DeploymentStopMonitoring{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID: deployment}
	return client.stanPublish(event, DeploymentStopMonitoringRequested, tid)
}

// StopMonitoring stops monitoring on a deployment.
func (client natsDeploymentClient) StopMonitoring(tid common.TransactionID, deployment common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.StopMonitoring",
	})
	if tid == "" {
		tid = messaging.NewTransactionID()
	}

	var resultChan = make(chan cloudevents.Event)
	listenerEventTypes := []common.EventType{DeploymentMonitoringStopped, DeploymentMonitoringStopFailed}
	listenerID, err := client.listenForResponse(listenerEventTypes, tid, resultChan)
	if err != nil {
		logger.WithError(err).Error("unable to listen for a response")
		return err
	}

	err = client.StopMonitoringAsync(tid, deployment)
	if err != nil {
		logger.WithError(err).Error("unable to stop deployment monitoring")
		client.eventSrc.RemoveListenerByID(listenerID)
		return err
	}

	var resultEvent DeploymentStopMonitoringResult
	receivedEventType, err := client.waitForResponse(listenerID, resultChan, &resultEvent)
	if err != nil {
		logger.WithError(err).Error("unable to receive a response")
		return err
	}

	serviceError := resultEvent.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return serviceError
	}

	if receivedEventType == DeploymentMonitoringStopped {
		logger.WithFields(log.Fields{"ID": resultEvent.ID}).Trace("received result")
		return nil
	}

	return NewCacaoGeneralError("failed to stop deployment monitoring")
}

func (client natsDeploymentClient) stanPublish(event interface{}, eventType common.EventType, tid common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.stanPublish",
	})
	sc, err := client.stanConnect()
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer sc.Close()

	ce, err := messaging.CreateCloudEventWithTransactionID(
		event,
		string(eventType),
		client.clientID(),
		tid,
	)
	if err != nil {
		errorMessage := "unable to create a cloud event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	payload, err := ce.MarshalJSON()
	if err != nil {
		errorMessage := "unable to marshal a cloud event to JSON"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}

	err = sc.Publish(common.EventsSubject, payload)
	if err != nil {
		errorMessage := "unable to publish an event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	return nil
}

func (client natsDeploymentClient) listenForResponse(eventTypes []common.EventType, tid common.TransactionID, resultChan chan<- cloudevents.Event) (messaging.ListenerID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.listenForResponse",
	})
	handler := func(ev common.EventType, ce cloudevents.Event) {
		resultChan <- ce
	}

	listenerID, err := client.eventSrc.AddListenerMultiEventType(
		eventTypes, tid, messaging.Listener{Callback: handler, ListenOnce: true})
	if err != nil {
		errorMessage := "unable to add a listener for events"
		logger.WithError(err).Error(errorMessage)
		return "", NewCacaoCommunicationError(errorMessage)
	}

	return listenerID, nil
}

func (client natsDeploymentClient) waitForResponse(listenerID messaging.ListenerID, resultChan <-chan cloudevents.Event, resultEvent interface{}) (common.EventType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.waitForResponse",
	})
	var resultCe cloudevents.Event
	select {
	case resultCe = <-resultChan:
	case <-time.After(client.eventTimeout):
		client.eventSrc.RemoveListenerByID(listenerID)

		errorMessage := "response timeout"
		logger.Error(errorMessage)
		return "", NewCacaoTimeoutError(errorMessage)
	}

	err := json.Unmarshal(resultCe.Data(), resultEvent)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to a cloud event"
		logger.WithError(err).Error(errorMessage)
		return common.EventType(resultCe.Type()), NewCacaoMarshalError(errorMessage)
	}
	return common.EventType(resultCe.Type()), nil
}

func (client natsDeploymentClient) natsRequest(ctx context.Context, query interface{}, queryType common.QueryOp) (*cloudevents.Event, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.natsRequest",
	})
	nc, err := client.natsConnect()
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}
	return natsRequest(ctx, nc, query, queryType, client.clientID())
}

func (client natsDeploymentClient) natsRequestMultiRelies(ctx context.Context, query interface{}, queryType common.QueryOp, replyCountExtractor func(*cloudevents.Event) int) ([]cloudevents.Event, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.natsRequestMultiRelies",
	})
	nc, err := client.natsConnect()
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}
	return natsRequestMultiRelies(ctx, nc, query, queryType, client.clientID(), replyCountExtractor)
}

// Sends a NATS requests, return the reply as a cloudevent.
// Only expect a single reply.
func natsRequest(ctx context.Context, nc *nats.Conn, query interface{}, queryType common.QueryOp, source string) (*cloudevents.Event, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsRequest",
	})
	ce, err := messaging.CreateCloudEvent(query, string(queryType), source)
	if err != nil {
		errorMessage := "unable to create a cloud event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}
	payload, err := ce.MarshalJSON()
	if err != nil {
		errorMessage := "unable to marshal a cloud event to JSON"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	// retrieve the loaded user from the nats ether
	newctx, cancel := context.WithTimeout(ctx, time.Second*5)
	msg, err := nc.RequestWithContext(newctx, string(queryType), payload)
	cancel() // no longer need the context, so cancel at this point
	if err != nil {
		errorMessage := "unable to request"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	// convert the message to a cloud event
	ce, err = messaging.ConvertNats(msg)
	if err != nil {
		errorMessage := "unable to convert a NATS message to a cloud event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	return &ce, nil
}

// Request a list of NATS replies that is streamed on the same reply subject.
// repliesCountExtractor is used to extract the total number of replies from the 1st reply.
func natsRequestMultiRelies(ctx context.Context, nc *nats.Conn, query interface{}, queryType common.QueryOp,
	source string, replyCountExtractor func(*cloudevents.Event) int) ([]cloudevents.Event, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsWorkspaceClient.natsRequestMultiRelies",
	})
	ce, err := messaging.CreateCloudEvent(query, string(queryType), source)
	if err != nil {
		errorMessage := "unable to create a cloud event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}
	payload, err := json.Marshal(ce)
	if err != nil {
		errorMessage := "unable to marshal a cloud event to JSON"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	replySubject := nats.NewInbox()
	sub, err := nc.SubscribeSync(replySubject)
	if err != nil {
		errorMessage := "unable to subscribe"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}
	defer sub.Unsubscribe()

	err = nc.PublishRequest(string(queryType), replySubject, payload)
	if err != nil {
		errorMessage := "unable to request"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	getNextReply := func(sub *nats.Subscription) (*cloudevents.Event, error) {
		msg, err := sub.NextMsgWithContext(ctx)
		if err != nil {
			errorMessage := "unable to get next message"
			logger.WithError(err).Error(errorMessage)
			return nil, NewCacaoCommunicationError(errorMessage)
		}

		// convert the message to a cloud event
		ce, err = messaging.ConvertNats(msg)
		if err != nil {
			errorMessage := "unable to convert a NATS message to a cloud event"
			logger.WithError(err).Error(errorMessage)
			return nil, NewCacaoCommunicationError(errorMessage)
		}
		return &ce, err
	}

	// 1st reply
	replyEvent, err := getNextReply(sub)
	if err != nil {
		return nil, err
	}

	totalReplyCount := replyCountExtractor(replyEvent) // get total number of replies from 1st reply
	if totalReplyCount < 1 {
		errorMessage := "bad totalReplyCount"
		logger.WithField("totalReplyCount", totalReplyCount).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}
	allReply := []cloudevents.Event{*replyEvent}

	// rest of replies
	for i := 1; i < totalReplyCount; i++ {
		replyEvent, err = getNextReply(sub)
		if err != nil {
			return nil, err
		}
		allReply = append(allReply, *replyEvent)
	}
	return allReply, nil
}
