package providers

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/cyverse/cacao-common/service"
)

// Client is the interface to the provider service.
type Client interface {
	Image(string) error
	ListImages(*ImageListingArgs) ([]Image, error)
	Flavor(string) error
	ListFlavors(*FlavorListingArgs) ([]Flavor, error)
}

// QueryOp is a type to represent query operations
type QueryOp string

// ImagesListOp is the operation for listing images.
const ImagesListOp QueryOp = "images.list"

// ImagesGetOp is the operation for getting a single image.
const ImagesGetOp QueryOp = "images.get"

// FlavorsListOp is the operation for listing flavors.
const FlavorsListOp QueryOp = "flavors.list"

// FlavorsGetOp is the operation for getting a single image.
const FlavorsGetOp QueryOp = "flavors.get"

// CachePopulateOp is the operation for forcing a cache population for a user.
const CachePopulateOp QueryOp = "cache.populate"

// CredGetterType is the type to represent a type of credentials retriever.
type CredGetterType string

// CredGetterString is the type for getting credentials from a string.
const CredGetterString CredGetterType = "string"

// CredGetterClient is the type for getting credentials from a client.
const CredGetterClient CredGetterType = "client"

// CredGetterError is the type for returning errors for every credentials retrieval attempt.
const CredGetterError CredGetterType = "error"

// Credentials represents auth information about the user.
type Credentials struct {
	ID           string      `json:"id" mapstructure:"id"`
	OpenStackEnv Environment `json:"openstack_env,omitempty" mapstructure:"openstack_env"`
}

type baseRequest struct {
	ProviderID    string          `json:"provider_id"`
	Session       service.Session `json:"session"`
	Op            QueryOp         `json:"op"`
	TransactionID string          `json:"transaction_id,omitempty"`
	Args          interface{}
}

// ListingRequest is the format for incoming requests to images,
// either singly or as a list. This gets parsed from the data contained
// in the OpenStackRequest.CloudEvent field.
type ListingRequest struct {
	baseRequest
}

// NewListingRequest is a constructor *ListingRequest.
func NewListingRequest(session *service.Session, transactionID string, op QueryOp, args interface{}) *ListingRequest {
	return &ListingRequest{
		baseRequest: baseRequest{
			Session:       *session,
			Op:            op,
			TransactionID: transactionID,
			Args:          args,
		},
	}
}

// UnmarshalJSON is implemented here so that the ListingRequest.Args field
// will be parsed as appropriate for the ListingRequest.Op field. This is
// needed because the type of the Args field is dependent on the value of
// the Op field.
func (l *ListingRequest) UnmarshalJSON(data []byte) error {
	var (
		err  error
		ok   bool
		temp baseRequest
	)

	if err = json.Unmarshal(data, &temp); err != nil {
		return err
	}
	l.Op = temp.Op
	l.ProviderID = temp.ProviderID
	l.Session = temp.Session
	l.TransactionID = temp.TransactionID

	args := temp.Args

	switch l.Op {
	case ImagesListOp:
		var i ImageListingArgs
		if err = mapstructure.Decode(args, &i); err != nil {
			return fmt.Errorf("cannot convert %+v to an ImageListingArgs", args)
		}
		l.Args = i
		break

	case FlavorsListOp:
		var f FlavorListingArgs
		if err = mapstructure.Decode(args, &f); err != nil {
			return fmt.Errorf("cannot convert %+v to a FlavorListingArgs", args)
		}
		l.Args = f
		break

	// Assume anything else that comes through takes a string arg.
	default:
		l.Args, ok = args.(string)
		if !ok {
			return fmt.Errorf("cannot cast %+v to a string", args)
		}
		break
	}

	return nil
}

// Hashable defines the interface for objects that want to make a checksum available.
type Hashable interface {
	Checksum() []byte
}

// Flavor represents the compute, memory and storage capacity of a computing
// instance in OpenStack (https://docs.openstack.org/nova/latest/user/flavors.html)
type Flavor struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	RAM       int64  `json:"ram"`
	Ephemeral int64  `json:"ephemeral"`
	VCPUs     int64  `json:"vcpus"`
	IsPublic  bool   `json:"is_public"`
	Disk      int64  `json:"disk"`
}

// FlavorListingArgs represents the available options that can be passed to
// `openstack flavor list`.
type FlavorListingArgs struct {
	Public  bool
	Private bool
	All     bool
	Limit   int64
}

// Checksum returns the hashed value of the string representation of
// the FlavorListingArgs.
func (f *FlavorListingArgs) Checksum() []byte {
	hasher := sha256.New()
	return hasher.Sum([]byte(f.String()))
}

// String implements the Stringer interface for FlavorListingArgs. Note: this
// is not the same as the command-line representation.
func (f *FlavorListingArgs) String() string {
	return fmt.Sprintf("%v %v %v %v", f.Public, f.Private, f.All, f.Limit)
}

// Environment contains a map of string keys and string values representing the environment
// in which OpenStack CLI commands should execute.  This is important for setting the
// appropriate environment variables for accessing OpenStack
type Environment map[string]string

// Image describes an image available through OpenStack. Adapted from the
// long listing format.
type Image struct {
	ID              string   `json:"id"`
	Name            string   `json:"name"`
	DiskFormat      string   `json:"disk_format"`
	ContainerFormat string   `json:"container_format"`
	Size            int64    `json:"size"`
	Checksum        string   `json:"checksum"`
	Status          string   `json:"status"`
	Visibility      string   `json:"visibility"`
	Protected       bool     `json:"protected"`
	Project         string   `json:"project"`
	Tags            []string `json:"tags"`
}

// ImageListingSortOption contains the key and direction for a sorting
// option passed to the openstack command-line.
type ImageListingSortOption struct {
	Key       string
	Direction string
}

// ImageListingArgs contains the possibly settings for listing images.
type ImageListingArgs struct {
	Public        bool
	Private       bool
	Community     bool
	Shared        bool
	Name          string
	Status        string
	MemberStatus  string
	Project       string
	ProjectDomain string
	Tag           string
	Limit         int64
	SortOpts      []ImageListingSortOption
	Properties    []string // should be in the key=value format
}

// Checksum implements the Hashable interface for ImageListingArgs.
func (i *ImageListingArgs) Checksum() []byte {
	hasher := sha256.New()
	return hasher.Sum([]byte(i.String()))
}

// String implements the String interface for ImageListingArgss. Note:
// this does not return the command-line arguments for the corresponding
// openstack command.
func (i *ImageListingArgs) String() string {
	return fmt.Sprintf("%v %v %v %v %v %v %v %v %v %v %v %v %v",
		i.Public,
		i.Private,
		i.Community,
		i.Shared,
		i.Name,
		i.Status,
		i.MemberStatus,
		i.Project,
		i.ProjectDomain,
		i.Tag,
		i.Limit,
		i.SortOpts,
		i.Properties,
	)
}

// StringArgs exists so that the command-line calls that only need
// a single custom argument of type string can still be used for
// generating a cache key.
type StringArgs string

// Checksum implements the Hashable interface for StringArgs and returns
// the SHA256 of the StringArgs.
func (s StringArgs) Checksum() []byte {
	hasher := sha256.New()
	return hasher.Sum([]byte(s))
}

// OpenStackProviderError is what gets returned when an error occurs during the
// processing of a request.
type OpenStackProviderError struct {
	Error         string `json:"error"`
	TransactionID string `json:"transaction_id"`
}
