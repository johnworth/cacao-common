package providers

import (
	"encoding/json"
	"strconv"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
)

// ParseCloudEvent parses out a cloud event from NATS message data.
func ParseCloudEvent(msgData []byte) (*cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	err := json.Unmarshal(msgData, &event)
	if err != nil {
		return nil, err
	}

	return &event, nil
}

// ParseEventBody unmarshals the JSON contained in the body of the cloudevent.
func ParseEventBody(event *cloudevents.Event, data interface{}) error {
	eventData := event.Data()
	unquoted, err := strconv.Unquote(string(eventData))
	if err != nil {
		return err
	}
	if err = json.Unmarshal([]byte(unquoted), data); err != nil {
		return err
	}
	return nil
}

// createCloudEvent takes in data as an interface{}, marshals it as JSON, and
// puts it into the data field of a newly created CloudEvent, which is then
// returned.
func createCloudEvent(data interface{}, op QueryOp, id, source string) (*cloudevents.Event, error) {
	encodedData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	event := cloudevents.NewEvent()
	event.SetID(id)
	event.SetType(string(op))
	event.SetTime(time.Now())
	event.SetSource(source)
	if err = event.SetData(cloudevents.ApplicationJSON, string(encodedData)); err != nil {
		return nil, err
	}

	return &event, nil
}
