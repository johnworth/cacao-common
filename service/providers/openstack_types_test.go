package providers

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
)

func TestListingRequestUnmarshalJSON(t *testing.T) {
	testValues := []ListingRequest{
		{
			baseRequest: baseRequest{
				Session: service.Session{
					SessionActor:    "test-user-0",
					SessionEmulator: "test-user-0",
				},
				Op:            ImagesListOp,
				TransactionID: "test-transaction-id-0",
				ProviderID:    "openstack",
				Args: ImageListingArgs{
					Public: true,
				},
			},
		},
		{
			baseRequest: baseRequest{
				Session: service.Session{
					SessionActor:    "test-user-1",
					SessionEmulator: "test-user-1",
				},
				Op:            FlavorsListOp,
				TransactionID: "test-transaction-id-1",
				ProviderID:    "openstack",
				Args: FlavorListingArgs{
					Private: true,
				},
			},
		},
		{
			baseRequest: baseRequest{
				Session: service.Session{
					SessionActor:    "test-user-2",
					SessionEmulator: "test-user-2",
				},
				Op:            ImagesGetOp,
				TransactionID: "test-transaction-id-2",
				ProviderID:    "openstack",
				Args:          "test-image-2",
			},
		},
		{
			baseRequest: baseRequest{
				Session: service.Session{
					SessionActor:    "test-user-3",
					SessionEmulator: "test-user-3",
				},
				Op:            ImagesGetOp,
				TransactionID: "test-transaction-id-3",
				ProviderID:    "openstack",
				Args:          "test-image-3",
			},
		},
		{
			baseRequest: baseRequest{
				Session: service.Session{
					SessionActor:    "test-user-4",
					SessionEmulator: "test-user-4",
				},
				Op:            CachePopulateOp,
				TransactionID: "test-transaction-id-4",
				ProviderID:    "openstack",
				Args:          "",
			},
		},
	}

	for index, val := range testValues {
		marshalled, err := json.Marshal(&val)
		if err != nil {
			t.Error(err)
		}

		actual := &ListingRequest{}
		if assert.NoError(t, actual.UnmarshalJSON(marshalled)) {
			expected := testValues[index]
			assert.Equal(t, &expected, actual)
		}
	}
}
