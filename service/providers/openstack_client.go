package providers

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/nats.go"
	"github.com/rs/xid"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
)

// OpenStackProvider is the interface for all provider services.
type OpenStackProvider interface {
	Init() error
	ImageList(ctx context.Context, session *service.Session) ([]Image, error)
	GetImage(ctx context.Context, session *service.Session, imageID string) (*Image, error)
	FlavorList(ctx context.Context, session *service.Session) ([]Flavor, error)
	GetFlavor(ctx context.Context, session *service.Session, flavorID string) (*Flavor, error)
	Finish() error
}

type providerMessage struct {
	session       *service.Session
	op            QueryOp
	providerID    string
	transactionID string
	subject       string
	replySubject  string
	args          interface{}
}

// natsOpenStack is an implementation of Provider that sends requests
// over NATS and waits for responses.
type natsOpenStack struct {
	ID             string
	conn           *nats.Conn
	credGetterType CredGetterType
	nats           *messaging.NatsConfig
	stan           *messaging.StanConfig
}

// NewOpenStackProvider returns a new instance of Provider. You will still need to call
// Init() on it.
func NewOpenStackProvider(
	id string,
	nats *messaging.NatsConfig,
	stan *messaging.StanConfig,
) OpenStackProvider {
	return &natsOpenStack{
		ID:             id,
		credGetterType: CredGetterClient,
		nats:           nats,
		stan:           stan,
	}
}

// Init sets up the connection to NATS.
func (p *natsOpenStack) Init() error {
	var err error
	p.conn, err = nats.Connect(p.nats.URL)
	return err
}

// Finish cleans up the connection to NATS.
func (p *natsOpenStack) Finish() error {
	defer p.conn.Close()
	return nil
}

// createNATSMsg takes the parameters and creates a new CloudEvent
// with them, which is then marshaled into JSON (thus doubly encoding
// the data in the CloudEvent) and is included as the data field in a new
// *nats.Msg.
func (p *natsOpenStack) createNATSMsg(
	ctx context.Context,
	providerMsg *providerMessage,
) (*nats.Msg, error) {
	ce, err := createCloudEvent(
		NewListingRequest(
			providerMsg.session,
			providerMsg.transactionID,
			providerMsg.op,
			providerMsg.args,
		), providerMsg.op, providerMsg.transactionID, "api.ProviderNATS")
	if err != nil {
		return nil, err
	}

	ceData, err := json.Marshal(ce)
	if err != nil {
		return nil, err
	}

	msg := &nats.Msg{
		Subject: providerMsg.subject,
		Reply:   providerMsg.replySubject,
		Data:    ceData,
	}
	return msg, nil
}

// sendMsg uses createNATSMsg() to create a new *nats.Msg and sends it to the openstack provider
// service. It then blocks, waiting for a response from the service. The context passed in should
// be used to limit the amount of time spent waiting for a response.
func (p *natsOpenStack) sendMsg(
	ctx context.Context,
	providerMsg *providerMessage,
) (*cloudevents.Event, error) {
	var (
		err error
		msg nats.Msg
	)
	replyChan := make(chan nats.Msg, 1) // Buffered with one to prevent losing a message.
	subHandler := func(m *nats.Msg) {
		replyChan <- *m
	}

	outgoingMsg, err := p.createNATSMsg(
		ctx,
		providerMsg,
	)
	if err != nil {
		return nil, err
	}

	// We're doing an async subscribe to make sure we don't lose the message.
	// Trying to do it synchronously in the same thread/goroutine that sends
	// the message was hurting my brain. Also, always subscribe first, then
	// send the message to prevent missing a response.
	if _, err = p.conn.Subscribe(providerMsg.replySubject, subHandler); err != nil {
		return nil, err
	}

	if err = p.conn.PublishMsg(outgoingMsg); err != nil {
		return nil, err
	}

	// The caller can (and should) set a timeout on the call, so
	// it's a race to see whether the call completes or the timeout gets called.
	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	case msg = <-replyChan:
		respStatusStr := msg.Header.Get("Status")
		cloudEvent, err := ParseCloudEvent(msg.Data)
		if err != nil {
			return nil, err
		}

		if respStatusStr != "" {
			respStatus, err := strconv.ParseInt(respStatusStr, 10, 32)
			if err != nil {
				return nil, err
			}
			if respStatus != 200 {
				// should have returned an error, so handle it here.
				errData := cloudEvent.Data()

				unquoted, err := strconv.Unquote(string(errData))
				if err != nil {
					return nil, err
				}

				var providerErr OpenStackProviderError
				if err = json.Unmarshal([]byte(unquoted), &providerErr); err != nil {
					return nil, err
				}

				return nil, fmt.Errorf(
					"transaction: %s, status: %d, msg: %s",
					providerErr.TransactionID,
					respStatus,
					providerErr.Error,
				)
			}
		}
		return cloudEvent, err
	}
}

// doOp sends the operation and arguments out across NATS, waits for a response,
// and parses out the event body into a data structure that's passed in. You'll
// want to make sure the output parameter is a pointer, that's not enforced here.
func (p *natsOpenStack) doOp(
	ctx context.Context,
	session *service.Session,
	op QueryOp,
	args interface{},
	output interface{},
) error {
	transactionID := xid.New().String()
	subject := fmt.Sprintf("cacao.providers.openstack.%s", string(op))
	replySubject := fmt.Sprintf("cacao.replies.%s", transactionID)
	providerMsg := &providerMessage{
		transactionID: transactionID,
		session:       session,
		providerID:    p.ID,
		subject:       subject,
		replySubject:  replySubject,
		args:          args,
		op:            op,
	}
	response, err := p.sendMsg(
		ctx,
		providerMsg,
	)
	if err != nil {
		return err
	}

	return ParseEventBody(response, output)
}

// Images gets the listing of available images and returns it.
func (p *natsOpenStack) ImageList(ctx context.Context, session *service.Session) ([]Image, error) {
	var (
		images []Image
		err    error
	)

	if err = p.doOp(
		ctx,
		session,
		ImagesListOp,
		ImageListingArgs{},
		&images,
	); err != nil {
		return nil, err
	}

	return images, nil
}

// Image returns a specific image by its UUID.
func (p *natsOpenStack) GetImage(ctx context.Context, session *service.Session, id string) (*Image, error) {
	var (
		image Image
		err   error
	)

	if err = p.doOp(
		ctx,
		session,
		ImagesGetOp,
		id,
		&image,
	); err != nil {
		return nil, err
	}

	return &image, nil
}

// Flavors returns a list of the available flavors.
func (p *natsOpenStack) FlavorList(ctx context.Context, session *service.Session) ([]Flavor, error) {
	var (
		flavors []Flavor
		err     error
	)

	if err = p.doOp(
		ctx,
		session,
		FlavorsListOp,
		FlavorListingArgs{},
		&flavors,
	); err != nil {
		return nil, err
	}

	return flavors, nil
}

// Flavor returns a specific image by its UUID.
func (p *natsOpenStack) GetFlavor(ctx context.Context, session *service.Session, id string) (*Flavor, error) {
	var (
		flavor Flavor
		err    error
	)
	if err = p.doOp(
		ctx,
		session,
		FlavorsGetOp,
		id,
		&flavor,
	); err != nil {
		return nil, err
	}

	return &flavor, nil
}
