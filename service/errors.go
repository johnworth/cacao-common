// Package service this file contains general error strings, types, or constants that are used within the service package
package service

import (
	"encoding/json"
	"errors"
	"fmt"
)

// Disclaimer:
// CacaoStandardErrorMessage, CacaoErrorBase, Cacao*Error types are introduced
// to define errors used between Microservices, Microservice Clients, and clients in API clients.
// These are currently used in Workspace Microservice and Template Microservice.

// CacaoStandardErrorMessage is an error message type, describing error type.
// It is also used to determine error type (e.g., CacaoMarshalError) from CacaoError interface.
type CacaoStandardErrorMessage string

// defines standard error messages
// the standard error messages are used to determine types of CacaoError and to describe error
const (
	CacaoMarshalErrorMessage           CacaoStandardErrorMessage = "marshal/unmarshal failed"
	CacaoUnauthorizedErrorMessage      CacaoStandardErrorMessage = "unauthorized access"
	CacaoTimeoutErrorMessage           CacaoStandardErrorMessage = "operation timedout error"
	CacaoOperationCanceledErrorMessage CacaoStandardErrorMessage = "operation canceled error"
	CacaoInvalidParameterErrorMessage  CacaoStandardErrorMessage = "invalid parameter error"
	CacaoNotFoundErrorMessage          CacaoStandardErrorMessage = "object not found"
	CacaoAlreadyExistErrorMessage      CacaoStandardErrorMessage = "object already exist"
	CacaoNotImplementedErrorMessage    CacaoStandardErrorMessage = "function not implemenated"
	CacaoCommunicationErrorMessage     CacaoStandardErrorMessage = "communication failed" // e.g., NATS request failed
	CacaoGeneralErrorMessage           CacaoStandardErrorMessage = "work failed"
)

// convertBaseToCacaoError converts CacaoErrorBase into CacaoError
// have a big switch to compare standard error messages and do type cast.
func convertBaseToCacaoError(base CacaoErrorBase) (CacaoError, error) {
	switch base.StandardMessage {
	case "":
		// no error
		return nil, nil
	case CacaoMarshalErrorMessage:
		return &CacaoMarshalError{
			CacaoErrorBase: base,
		}, nil
	case CacaoUnauthorizedErrorMessage:
		return &CacaoUnauthorizedError{
			CacaoErrorBase: base,
		}, nil
	case CacaoTimeoutErrorMessage:
		return &CacaoTimeoutError{
			CacaoErrorBase: base,
		}, nil
	case CacaoOperationCanceledErrorMessage:
		return &CacaoOperationCanceledError{
			CacaoErrorBase: base,
		}, nil
	case CacaoInvalidParameterErrorMessage:
		return &CacaoInvalidParameterError{
			CacaoErrorBase: base,
		}, nil
	case CacaoNotFoundErrorMessage:
		return &CacaoNotFoundError{
			CacaoErrorBase: base,
		}, nil
	case CacaoAlreadyExistErrorMessage:
		return &CacaoAlreadyExistError{
			CacaoErrorBase: base,
		}, nil
	case CacaoNotImplementedErrorMessage:
		return &CacaoNotImplementedError{
			CacaoErrorBase: base,
		}, nil
	case CacaoCommunicationErrorMessage:
		return &CacaoCommunicationError{
			CacaoErrorBase: base,
		}, nil
	default:
		return nil, fmt.Errorf("cannot convert CacaoErrorBase %s to CacaoError", base.StandardMessage)
	}
}

// convertJSONToCacaoError converts JSON bytes into CacaoError
// have a big switch to compare standard error messages and do unmarshal.
func convertJSONToCacaoError(message CacaoStandardErrorMessage, jsonBytes []byte) (CacaoError, error) {
	var err error
	switch message {
	case CacaoMarshalErrorMessage:
		var cerror CacaoMarshalError
		err = json.Unmarshal(jsonBytes, &cerror)
		if err != nil {
			return nil, err
		}
		return &cerror, nil
	case CacaoUnauthorizedErrorMessage:
		var cerror CacaoUnauthorizedError
		err = json.Unmarshal(jsonBytes, &cerror)
		if err != nil {
			return nil, err
		}
		return &cerror, nil
	case CacaoTimeoutErrorMessage:
		var cerror CacaoTimeoutError
		err = json.Unmarshal(jsonBytes, &cerror)
		if err != nil {
			return nil, err
		}
		return &cerror, nil
	case CacaoOperationCanceledErrorMessage:
		var cerror CacaoOperationCanceledError
		err = json.Unmarshal(jsonBytes, &cerror)
		if err != nil {
			return nil, err
		}
		return &cerror, nil
	case CacaoInvalidParameterErrorMessage:
		var cerror CacaoInvalidParameterError
		err = json.Unmarshal(jsonBytes, &cerror)
		if err != nil {
			return nil, err
		}
		return &cerror, nil
	case CacaoNotFoundErrorMessage:
		var cerror CacaoNotFoundError
		err = json.Unmarshal(jsonBytes, &cerror)
		if err != nil {
			return nil, err
		}
		return &cerror, nil
	case CacaoAlreadyExistErrorMessage:
		var cerror CacaoAlreadyExistError
		err = json.Unmarshal(jsonBytes, &cerror)
		if err != nil {
			return nil, err
		}
		return &cerror, nil
	case CacaoNotImplementedErrorMessage:
		var cerror CacaoNotImplementedError
		err = json.Unmarshal(jsonBytes, &cerror)
		if err != nil {
			return nil, err
		}
		return &cerror, nil
	case CacaoCommunicationErrorMessage:
		var cerror CacaoCommunicationError
		err = json.Unmarshal(jsonBytes, &cerror)
		if err != nil {
			return nil, err
		}
		return &cerror, nil
	default:
		// unknown, cast to CacaoErrorBase
		var base CacaoErrorBase
		err = json.Unmarshal(jsonBytes, &base)
		if err != nil {
			return nil, err
		}
		return &base, nil
	}
}

// CacaoErrorOptions is an option that stores additional information of the error
type CacaoErrorOptions struct {
	Temporary bool `json:"temporary,omitempty"` // set true if the error is temporary one
	// TODO: add more here
}

// CacaoErrorOption is a option type for CacaoError
type CacaoErrorOption func(options *CacaoErrorOptions)

// getDefaultCacaoErrorOptions returns default option values
func getDefaultCacaoErrorOptions() CacaoErrorOptions {
	return CacaoErrorOptions{
		Temporary: false,
	}
}

func getCacaoErrorOptions(options ...CacaoErrorOption) CacaoErrorOptions {
	opts := getDefaultCacaoErrorOptions()
	// overwrites
	for _, opt := range options {
		if opt != nil {
			opt(&opts)
		}
	}
	return opts
}

// CacaoErrorTemporary creates an option for CacaoError meaning that the error is temporary
func CacaoErrorTemporary(val bool) CacaoErrorOption {
	return func(options *CacaoErrorOptions) {
		options.Temporary = val
	}
}

// CacaoError is for cacao service errors.
type CacaoError interface {
	// Error returns error message that includes both contextual and standard error messages.
	Error() string
	// ContextualError returns contextual error message. e.g., "actor 'edwin' is not authorized to create a user"
	ContextualError() string
	// StandardError returns standard error message. e.g., "unauthorize access"
	StandardError() CacaoStandardErrorMessage
	// IsTemporary tells if the error is transitive meaning that you may get different result at next call.
	IsTemporary() bool
	// MarshalJSON marshals the error to JSON bytes
	MarshalJSON() ([]byte, error)
	// GetBase returns CacaoErrorBase type
	GetBase() CacaoErrorBase
}

// CacaoErrorBase is a struct for base cacao service error type.
type CacaoErrorBase struct {
	StandardMessage   CacaoStandardErrorMessage `json:"standard_msg"`
	ContextualMessage string                    `json:"contextual_msg,omitempty"`
	Options           CacaoErrorOptions         `json:"options,omitempty"`
}

// Error returns a message
func (err *CacaoErrorBase) Error() string {
	return fmt.Sprintf("%s - %s", string(err.StandardError()), err.ContextualMessage)
}

// ContextualError returns a contextual error message
func (err *CacaoErrorBase) ContextualError() string {
	return err.ContextualMessage
}

// StandardError returns a standard error message
func (err *CacaoErrorBase) StandardError() CacaoStandardErrorMessage {
	return err.StandardMessage
}

// IsTemporary returns whether the error is temporary
func (err *CacaoErrorBase) IsTemporary() bool {
	return err.Options.Temporary
}

// MarshalJSON returns JSON bytes
func (err *CacaoErrorBase) MarshalJSON() ([]byte, error) {
	return json.Marshal(*err)
}

// GetBase returns CacaoErrorBase type
func (err *CacaoErrorBase) GetBase() CacaoErrorBase {
	return *err
}

// SetContextualError sets a contextual error message
func (err *CacaoErrorBase) SetContextualError(message string) {
	err.ContextualMessage = message
}

// SetStandardError sets a standard error message
func (err *CacaoErrorBase) SetStandardError(message CacaoStandardErrorMessage) {
	err.StandardMessage = message
}

// SetOptions sets options
func (err *CacaoErrorBase) SetOptions(options ...CacaoErrorOption) {
	for _, opt := range options {
		if opt != nil {
			opt(&err.Options)
		}
	}
}

// SetOptions sets options
func (err *CacaoErrorBase) ReplaceOptions(options CacaoErrorOptions) {
	err.Options = options
}

// ConvertJSONToCacaoError converts JSON bytes into CacaoError
func ConvertJSONToCacaoError(jsonBytes []byte) (CacaoError, error) {
	var jsonMap map[string]interface{}
	err := json.Unmarshal(jsonBytes, &jsonMap)
	if err != nil {
		// failed to unmarshal
		return nil, err
	}

	// check standard_msg field
	if message, ok := jsonMap["standard_msg"]; ok {
		// valid CacaoError
		if standardErrorMessage, ok2 := message.(string); ok2 {
			return convertJSONToCacaoError(CacaoStandardErrorMessage(standardErrorMessage), jsonBytes)
		}
		return nil, errors.New("could not convert JSON into CacaoError, 'standard_msg' field is not string type")
	}
	return nil, errors.New("could not convert JSON into CacaoError, 'standard_msg' field is not found")
}

// CacaoMarshalError is an error for marshal and unmarshal.
type CacaoMarshalError struct {
	CacaoErrorBase
}

// NewCacaoMarshalError creates a new CacaoMarshalError
func NewCacaoMarshalError(contextualMsg string) CacaoError {
	err := CacaoMarshalError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoMarshalErrorMessage)
	err.ReplaceOptions(getDefaultCacaoErrorOptions())
	return &err
}

// NewCacaoMarshalErrorWithOptions creates a new CacaoMarshalError with options
func NewCacaoMarshalErrorWithOptions(contextualMsg string, options ...CacaoErrorOption) CacaoError {
	err := CacaoMarshalError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoMarshalErrorMessage)
	err.ReplaceOptions(getCacaoErrorOptions(options...))
	return &err
}

// CacaoUnauthorizedError is an error for unauthorized user/access
type CacaoUnauthorizedError struct {
	CacaoErrorBase
}

// NewCacaoUnauthorizedError creates a new CacaoUnauthorizedError
func NewCacaoUnauthorizedError(contextualMsg string) CacaoError {
	err := CacaoUnauthorizedError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoUnauthorizedErrorMessage)
	err.ReplaceOptions(getDefaultCacaoErrorOptions())
	return &err
}

// NewCacaoUnauthorizedErrorWithOptions creates a new CacaoUnauthorizedError with options
func NewCacaoUnauthorizedErrorWithOptions(contextualMsg string, options ...CacaoErrorOption) CacaoError {
	err := CacaoUnauthorizedError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoUnauthorizedErrorMessage)
	err.ReplaceOptions(getCacaoErrorOptions(options...))
	return &err
}

// CacaoTimeoutError is an error for operation timeout
type CacaoTimeoutError struct {
	CacaoErrorBase
}

// NewCacaoTimeoutError creates a new CacaoTimeoutError
func NewCacaoTimeoutError(contextualMsg string) CacaoError {
	err := CacaoTimeoutError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoTimeoutErrorMessage)
	err.ReplaceOptions(getDefaultCacaoErrorOptions())
	return &err
}

// NewCacaoTimeoutErrorWithOptions creates a new CacaoTimeoutError with options
func NewCacaoTimeoutErrorWithOptions(contextualMsg string, options ...CacaoErrorOption) CacaoError {
	err := CacaoTimeoutError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoTimeoutErrorMessage)
	err.ReplaceOptions(getCacaoErrorOptions(options...))
	return &err
}

// CacaoOperationCanceledError is an error for operation canceled
type CacaoOperationCanceledError struct {
	CacaoErrorBase
}

// NewCacaoOperationCanceledError creates a new CacaoOperationCanceledError
func NewCacaoOperationCanceledError(contextualMsg string) CacaoError {
	err := CacaoOperationCanceledError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoOperationCanceledErrorMessage)
	err.ReplaceOptions(getDefaultCacaoErrorOptions())
	return &err
}

// NewCacaoOperationCanceledErrorWithOptions creates a new CacaoOperationCanceledError with options
func NewCacaoOperationCanceledErrorWithOptions(contextualMsg string, options ...CacaoErrorOption) CacaoError {
	err := CacaoOperationCanceledError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoOperationCanceledErrorMessage)
	err.ReplaceOptions(getCacaoErrorOptions(options...))
	return &err
}

// CacaoInvalidParameterError is an error for invalid input parameter
type CacaoInvalidParameterError struct {
	CacaoErrorBase
}

// NewCacaoInvalidParameterError creates a new CacaoInvalidParameterError
func NewCacaoInvalidParameterError(contextualMsg string) CacaoError {
	err := CacaoInvalidParameterError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoInvalidParameterErrorMessage)
	err.ReplaceOptions(getDefaultCacaoErrorOptions())
	return &err
}

// NewCacaoInvalidParameterErrorWithOptions creates a new CacaoInvalidParameterError with options
func NewCacaoInvalidParameterErrorWithOptions(contextualMsg string, options ...CacaoErrorOption) CacaoError {
	err := CacaoInvalidParameterError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoInvalidParameterErrorMessage)
	err.ReplaceOptions(getCacaoErrorOptions(options...))
	return &err
}

// CacaoNotFoundError is an error for object not found
type CacaoNotFoundError struct {
	CacaoErrorBase
}

// NewCacaoNotFoundError creates a new CacaoNotFoundError
func NewCacaoNotFoundError(contextualMsg string) CacaoError {
	err := CacaoNotFoundError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoNotFoundErrorMessage)
	err.ReplaceOptions(getDefaultCacaoErrorOptions())
	return &err
}

// NewCacaoNotFoundErrorWithOptions creates a new CacaoNotFoundError with options
func NewCacaoNotFoundErrorWithOptions(contextualMsg string, options ...CacaoErrorOption) CacaoError {
	err := CacaoNotFoundError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoNotFoundErrorMessage)
	err.ReplaceOptions(getCacaoErrorOptions(options...))
	return &err
}

// CacaoAlreadyExistError is an error for object already exist
type CacaoAlreadyExistError struct {
	CacaoErrorBase
}

// NewCacaoAlreadyExistError creates a new CacaoAlreadyExistError
func NewCacaoAlreadyExistError(contextualMsg string) CacaoError {
	err := CacaoAlreadyExistError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoAlreadyExistErrorMessage)
	err.ReplaceOptions(getDefaultCacaoErrorOptions())
	return &err
}

// NewCacaoAlreadyExistErrorWithOptions creates a new CacaoAlreadyExistError with options
func NewCacaoAlreadyExistErrorWithOptions(contextualMsg string, options ...CacaoErrorOption) CacaoError {
	err := CacaoAlreadyExistError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoAlreadyExistErrorMessage)
	err.ReplaceOptions(getCacaoErrorOptions(options...))
	return &err
}

// CacaoNotImplementedError is an error for function not implemented
type CacaoNotImplementedError struct {
	CacaoErrorBase
}

// NewCacaoNotImplementedError creates a new CacaoNotImplementedError
func NewCacaoNotImplementedError(contextualMsg string) CacaoError {
	err := CacaoNotImplementedError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoNotImplementedErrorMessage)
	err.ReplaceOptions(getDefaultCacaoErrorOptions())
	return &err
}

// NewCacaoNotImplementedErrorWithOptions creates a new CacaoNotImplementedError with options
func NewCacaoNotImplementedErrorWithOptions(contextualMsg string, options ...CacaoErrorOption) CacaoError {
	err := CacaoNotImplementedError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoNotImplementedErrorMessage)
	err.ReplaceOptions(getCacaoErrorOptions(options...))
	return &err
}

// CacaoCommunicationError is an error for communication failure.
type CacaoCommunicationError struct {
	CacaoErrorBase
}

// NewCacaoCommunicationError creates a new CacaoCommunicationError
func NewCacaoCommunicationError(contextualMsg string) CacaoError {
	err := CacaoCommunicationError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoCommunicationErrorMessage)
	err.ReplaceOptions(getDefaultCacaoErrorOptions())
	return &err
}

// NewCacaoCommunicationErrorWithOptions creates a new CacaoCommunicationError with options
func NewCacaoCommunicationErrorWithOptions(contextualMsg string, options ...CacaoErrorOption) CacaoError {
	err := CacaoCommunicationError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoCommunicationErrorMessage)
	err.ReplaceOptions(getCacaoErrorOptions(options...))
	return &err
}

// CacaoGeneralError is an error for general failure.
type CacaoGeneralError struct {
	CacaoErrorBase
}

// NewCacaoGeneralError creates a new CacaoGeneralError
func NewCacaoGeneralError(contextualMsg string) CacaoError {
	err := CacaoGeneralError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoGeneralErrorMessage)
	err.ReplaceOptions(getDefaultCacaoErrorOptions())
	return &err
}

// NewCacaoGeneralErrorWithOptions creates a new CacaoGeneralError with options
func NewCacaoGeneralErrorWithOptions(contextualMsg string, options ...CacaoErrorOption) CacaoError {
	err := CacaoGeneralError{}
	err.SetContextualError(contextualMsg)
	err.SetStandardError(CacaoGeneralErrorMessage)
	err.ReplaceOptions(getCacaoErrorOptions(options...))
	return &err
}

// OLD Error types
// TODO: need to move these error strings to service implementations

// these general errors strings, perhaps applicable to multiple service
// to create a new error object from this string, call error.New(string)
const (
	GeneralUnmarshalFromUserMSError     string = "could not unmarshal User object on User microservice side"
	GeneralEventOpTimeoutError          string = "event operation timeout"
	GeneralEventOpCanceledError         string = "event operation was canceled"
	GeneralActorNotAuthorizedError      string = "actor not authorized"
	GeneralActorNotFoundError           string = "actor was not found; perhaps you can use the bypass option"
	GeneralMethodNotYetImplementedError string = "method not yet implemented"
)

// these are user object type of errors
// to create a new error, call error.New(string)
const (
	UserUsernameNotSetError            string = "username was not set before using User" // if username not initialized before Load
	UserActorNotSetError               string = "actor was not set"
	UserUsernameNotFoundError          string = "username not found"
	UserUsernameReservedCannotAddError string = "username is reserved, cannot add user"
	UserUsernameExistsCannotAddError   string = "username exists, cannot add user"
	UserUpdateError                    string = "user could not be updated"
	UserDeleteError                    string = "user could not be deleted"
)

// these are provider object type of errors
// to create a new error, call error.New(string)
const (
	ProviderActorNotSetError string = "actor was not set"
	ProviderIDNotSetError    string = "provide id was not set"
)

// these are user list types of errors
// to create a new error, call error.New(string)
const (
	UserListFilterNotSetError            string = "user list filter was set before Search/SearchNext"
	UserListFilterInvalidStartIndexError string = "user list filter has invalid start index"
	UserListFilterInvalidMaxItemsError   string = "user list filter has invalid max items"
	UserListLoadBoundaryError            string = "next start index is invalid or no more elements to load beyond end of user list"
	UserListEmptyListError               string = "user list results size 0"
)

// these are just warning strings
// to create a new error, call error.New(string)
const (
	UserListFilterValueEmptyWarning string = "Warning UserFilter value was not set, will return all"
)
