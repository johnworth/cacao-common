// Package service - this package file will contain base service interfaces and structs for embedding into other service packages
package service

// IBaseObject is the base interface for objects for microservices
type SessionContext interface {
	GetSessionActor() string    // username of the person making requests
	GetSessionEmulator() string // set if the Actor's is being emulated by another user, presumably an admin
	SetSessionActor(string)
	SetSessionEmulator(string)
	GetError() (string, string) // gets the error type and message
	GetServiceError() CacaoError
}

type Session struct {
	SessionActor    string         `json:"actor"`
	SessionEmulator string         `json:"emulator,omitempty"`
	ErrorType       string         `json:"error_type,omitempty"`    // the type of error
	ErrorMessage    string         `json:"error_message,omitempty"` // custom or contextual error message
	ServiceError    CacaoErrorBase `json:"service_error,omitempty"` // Ultimately, this will replace ErrorType and ErrorMessage fields above.
}

func (b *Session) SetSessionActor(s string) {
	b.SessionActor = s
}
func (b *Session) SetSessionEmulator(s string) {
	b.SessionEmulator = s
}
func (b Session) GetSessionActor() string {
	return b.SessionActor
}
func (b Session) GetSessionEmulator() string {
	return b.SessionEmulator
}
func (b Session) GetError() (string, string) {
	return b.ErrorType, b.ErrorMessage
}

func (b Session) GetServiceError() CacaoError {
	if len(b.ServiceError.StandardMessage) == 0 {
		// no error
		return nil
	}

	cerr, err := convertBaseToCacaoError(b.ServiceError)
	if err != nil {
		// CacaoErrorBase type
		return &b.ServiceError
	}

	// specific Cacao*Error type
	return cerr
}
