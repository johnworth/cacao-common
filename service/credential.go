package service

import (
	"errors"
	"time"
)

const (
//DefaultCredentialListItemsMax = 10 // Default maximum number of Credential items to return
)

type SecretType string

type CredentialClient interface {
	Add(secret Credential) error
	Update(secret Credential) error
	Get(ID string) (Credential, error)
	Delete(ID string) error
	List() ([]Credential, error)
}

type Credential interface {
	SessionContext
	GetPrimaryID() string // points to username+"/"+id
	GetUsername() string
	GetValue() string
	GetType() SecretType
	GetID() string

	GetCreatedAt() time.Time
	GetUpdatedAt() time.Time
	GetUpdatedBy() string
	GetUpdatedEmulatorBy() string

	SetUsername(string)
	SetValue(string)
	SetType(SecretType)
	SetID(string)
	Verify() (bool, error)
}

type CredentialModel struct {
	Session
	Username          string     `json:"username,omitempty"`
	Value             string     `json:"value,omitempty"`
	Type              SecretType `json:"type,omitempty"`
	ID                string     `json:"id,omitempty"`
	CreatedAt         time.Time  `json:"created_at"`
	UpdatedAt         time.Time  `json:"updated_at"`
	UpdatedBy         string     `json:"updated_by"`
	UpdatedEmulatorBy string     `json:"updated_emulator_by"`
}

// GetRedacted will return a new Secret with the Value set to "REDACTED"
func (c *CredentialModel) GetRedacted() CredentialModel {
	redactedSecret := *c
	redactedSecret.Value = "REDACTED"
	return redactedSecret
}

func (c *CredentialModel) Verify() (bool, error) {
	if len(c.Username) <= 0 {
		return false, errors.New("username not set, could not verify")
	}
	if len(c.Value) <= 0 {
		return false, errors.New("value not set, could not verify")
	}
	if len(c.ID) <= 0 {
		return false, errors.New("ID not set, could not verify")
	}

	if len(c.Type) <= 0 {
		return false, errors.New("Type not set, could not verify")
	}

	return true, nil
}

func (c *CredentialModel) SetUsername(s string) {
	if c.Username != s {
		c.Username = s
	}
}

func (c *CredentialModel) SetValue(s string) {
	if c.Value != s {
		c.Value = s
	}
}

func (c *CredentialModel) SetType(s SecretType) {
	if c.Type != s {
		c.Type = s
	}
}

func (c *CredentialModel) SetID(s string) {
	if c.ID != s {
		c.ID = s
	}
}

func (c CredentialModel) GetPrimaryID() string {
	return c.Username + "/" + c.ID
}
func (c CredentialModel) GetUsername() string {
	return c.Username
}
func (c CredentialModel) GetCreatedAt() time.Time {
	return c.CreatedAt
}
func (c CredentialModel) GetUpdatedAt() time.Time {
	return c.CreatedAt
}
func (c CredentialModel) GetUpdatedBy() string {
	return c.UpdatedBy
}
func (c CredentialModel) GetUpdatedEmulatorBy() string {
	return c.UpdatedEmulatorBy
}

func (c CredentialModel) GetValue() string {
	return c.Value
}

func (c CredentialModel) GetType() SecretType {
	return c.Type
}

func (c CredentialModel) GetID() string {
	return c.ID
}
