package service

import (
	"net"

	"github.com/google/uuid"
	"gitlab.com/cyverse/cacao-common/common"
)

// DeploymentStateView is a higher level view of raw state
type DeploymentStateView struct {
	Resources []DeploymentResource `json:"resources"`
}

// DeploymentResource is a type of resource
type DeploymentResource struct {
	ID                  string                     `json:"id"`
	Type                DeploymentResourceType     `json:"type"`
	ProviderType        string                     `json:"provider_type"`
	Provider            common.ID                  `json:"provider"`
	Attributes          map[string]interface{}     `json:"attributes"`
	SensitiveAttributes []interface{}              `json:"sensitive_attributes"`
	AvailableActions    []DeploymentResourceAction `json:"available_actions"`
}

// DeploymentResourceAction is actions that can be performed on resource level or resource instance level
type DeploymentResourceAction string

const (
	// OpenWebShell ...
	OpenWebShell DeploymentResourceAction = "web_shell"

	// OpenWebDesktop ...
	OpenWebDesktop DeploymentResourceAction = "web_desktop"

	// OpenStackStartInstance ...
	OpenStackStartInstance DeploymentResourceAction = "start"

	// OpenStackShutDownInstance ...
	OpenStackShutDownInstance DeploymentResourceAction = "shut_down"

	// OpenStackShelveInstance ...
	OpenStackShelveInstance DeploymentResourceAction = "shelve"

	// OpenStackUnshelveInstance ...
	OpenStackUnshelveInstance DeploymentResourceAction = "unshelve"

	// OpenStackSuspendInstance ...
	OpenStackSuspendInstance DeploymentResourceAction = "suspend"

	// OpenStackResumeInstance ...
	OpenStackResumeInstance DeploymentResourceAction = "resume"
)

// DeploymentResourceType is the name of the resource type
type DeploymentResourceType string

// OpenStackInstance ...
const OpenStackInstance DeploymentResourceType = "openstack_instance"

// OpenStackInstanceAttributes corresponds to the Attributes in DeploymentResourceInstance
type OpenStackInstanceAttributes struct {
	ID         uuid.UUID   `json:"id"` // FIXME rebundant field? There is already ID field in DeploymentResource
	Name       string      `json:"name"`
	FloatingIP net.IP      `json:"floating_ip"`
	Image      string      `json:"image"`
	ImageID    uuid.UUID   `json:"image_id"`
	Flavor     string      `json:"flavor"`
	FlavorID   uuid.UUID   `json:"flavor_id"`
	Status     string      `json:"status"`
	Volumes    []uuid.UUID `json:"volumes,omitempty"`
}

// OpenStackVolume ...
const OpenStackVolume DeploymentResourceType = "openstack_volume"

// OpenStackVolumeAttributes corresponds to the Attributes in DeploymentResourceInstance
type OpenStackVolumeAttributes struct {
	ID   uuid.UUID `json:"id"` // FIXME rebundant field?
	Name string    `json:"name"`
	Size uint64    `json:"size"`
}
