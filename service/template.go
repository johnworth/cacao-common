package service

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
)

// TemplateFormat is the format of a template
type TemplateFormat string

const (
	// TemplateFormatYAML is for a template in yaml format
	TemplateFormatYAML TemplateFormat = "yaml"
	// TemplateFormatJSON is for a template in json format
	TemplateFormatJSON TemplateFormat = "json"
	// TemplateFormatJSON is for a template in hcl format
	TemplateFormatHCL TemplateFormat = "hcl"
)

// TemplateEngine is the type of a template engine
type TemplateEngine string

const (
	TemplateEngineTerraform TemplateEngine = "terraform"
)

// TemplateProviderType is the type of a template provider
type TemplateProviderType string

const (
	// TemplateProviderTypeOpenStack is for a template for Openstack provider
	TemplateProviderTypeOpenStack TemplateProviderType = "openstack"
	// TemplateProviderTypeAWS is for a template for AWS provider
	TemplateProviderTypeAWS TemplateProviderType = "aws"
	// TemplateProviderTypeGCP is for a template for GCP provider
	TemplateProviderTypeGCP TemplateProviderType = "gcp"
	// TemplateProviderTypeAzure is for a template for Azure provider
	TemplateProviderTypeAzure TemplateProviderType = "azure"
	// TemplateProviderTypeKubernetes is for a template for Kubernetes provider
	TemplateProviderTypeKubernetes TemplateProviderType = "kubernetes"
)

// TemplateSourceType is the type of a template source
type TemplateSourceType string

// TemplateSourceCredential is a struct for storing credentials to access template source
type TemplateSourceCredential struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// TemplateSourceVisiblity is the visibility of a template source
type TemplateSourceVisibility string

const (
	// TemplateSourceVisibilityPublic is for a public template source, no authentication is required to access the source
	TemplateSourceVisibilityPublic TemplateSourceVisibility = "public"
	// TemplateSourceVisibilityPrivate is for a private template source, the user’s credentials must be used to access the source
	TemplateSourceVisibilityPrivate TemplateSourceVisibility = "private"
)

// TemplateMetadata is a struct for template source information
type TemplateSource struct {
	Type TemplateSourceType `json:"type"`
	URI  string             `json:"uri"` // e.g., 'github.com/cyverse/sometemplate'
	// AccessParamters is used to pass additional parameters for accessing source (e.g., git branch name ...)
	// e.g., for git,
	// "branch": "master"
	// "tag": "tag_test"
	// "path": "/sub_dir_to_template_in_repo"
	AccessParameters map[string]interface{}   `json:"access_parameters"`
	Visibility       TemplateSourceVisibility `json:"source_visibility"` // e.g., 'public' or 'private'
}

// CacaoPostTask is a struct for a post task in metadata
type CacaoPostTask struct {
	Type         string `json:"type"`
	LocationType string `json:"location_type"`
}

// TemplateMetadata is a struct for template metadata
// The struct is generated from a metadata file (e.g., https://github.com/cyverse/tf-openstack-single-image/blob/master/metadata.json)
type TemplateMetadata struct {
	Name             string          `json:"name,omitempty"`
	Author           string          `json:"author,omitempty"`
	AuthorEmail      string          `json:"author_email,omitempty"`
	Description      string          `json:"description,omitempty"`
	TemplateTypeName string          `json:"template_type,omitempty"`
	CacaoPostTasks   []CacaoPostTask `json:"cacao_post_tasks,omitempty"`
	// Parameters stores template parameters
	// e.g.,
	// "instance_count": {"default": 1, "type": "integer"}
	// "instance_name": {"type": "string"},
	Parameters map[string]interface{} `json:"parameters,omitempty"`
}

// TemplateClient is the client for Template
type TemplateClient interface {
	// Source Types
	ListSourceTypes() ([]TemplateSourceType, error)

	// Types
	GetType(templateTypeName string) (TemplateType, error)
	ListTypes() ([]TemplateType, error)
	ListTypesForProviderType(providerType TemplateProviderType) ([]TemplateType, error)

	CreateType(templateType TemplateType) error
	UpdateType(templateType TemplateType) error
	UpdateTypeFields(templateType TemplateType, updateFieldNames []string) error
	DeleteType(templateTypeName string) error

	// Template
	List() ([]Template, error)
	Get(templateID common.ID) (Template, error)

	Import(template Template, credentialID string) (common.ID, error)
	Update(template Template) error
	UpdateFields(template Template, updateFieldNames []string) error
	Sync(templateID common.ID, credentialID string) error
	Delete(templateID common.ID) error
}

// TemplateType is the standard interface for all TemplateType implementations
type TemplateType interface {
	SessionContext

	GetPrimaryID() string // points to Name
	GetName() string
	GetFormats() []TemplateFormat
	GetEngine() TemplateEngine
	GetProviderTypes() []TemplateProviderType
	GetUpdateFieldNames() []string

	SetPrimaryID(id string)
	SetName(name string)
	SetFormats(formats []TemplateFormat)
	SetEngine(engine TemplateEngine)
	SetProviderTypes(providerTypes []TemplateProviderType)
	SetUpdateFieldNames(fieldNames []string)
}

// Template is the standard interface for all Template implementations
type Template interface {
	SessionContext

	GetPrimaryID() common.ID // points to ID
	GetID() common.ID
	GetOwner() string
	GetName() string
	GetDescription() string
	IsPublic() bool
	GetSource() TemplateSource
	GetMetadata() TemplateMetadata
	GetCreatedAt() time.Time
	GetUpdatedAt() time.Time
	GetUpdateFieldNames() []string

	SetPrimaryID(id common.ID)
	SetID(id common.ID)
	SetName(name string)
	SetDescription(description string)
	SetPublic(isPublic bool)
	SetSource(source TemplateSource)
	SetUpdateFieldNames(fieldNames []string)
}

// TemplateTypeListModel is the template type list model
type TemplateSourceTypeListModel struct {
	Session

	TemplateSourceTypes []TemplateSourceType `json:"template_source_types"`
}

// GetTemplateSourceTypes returns template source types
func (t *TemplateSourceTypeListModel) GetTemplateSourceTypes() []TemplateSourceType {
	return t.TemplateSourceTypes
}

// TemplateType is a type of a template and is only compatible to certain cloud providers.
type TemplateTypeModel struct {
	Session

	Name          string                 `json:"name"`
	Formats       []TemplateFormat       `json:"formats,omitempty"`        // e.g., {'yaml', 'json'}
	Engine        TemplateEngine         `json:"engine"`                   // e.g., 'terraform'
	ProviderTypes []TemplateProviderType `json:"provider_types,omitempty"` // e.g., {'openstack', 'aws', `kubernetes`}

	UpdateFieldNames []string `json:"update_field_names,omitempty"`
}

// GetPrimaryID returns the primary ID of the TemplateType
func (t *TemplateTypeModel) GetPrimaryID() string {
	return t.Name
}

// GetName returns the name of the TemplateType
func (t *TemplateTypeModel) GetName() string {
	return t.Name
}

// GetFormats returns the formats supported by the TemplateType
func (t *TemplateTypeModel) GetFormats() []TemplateFormat {
	return t.Formats
}

// GetEngine returns the engine for the TemplateType
func (t *TemplateTypeModel) GetEngine() TemplateEngine {
	return t.Engine
}

// GetProviderTypes returns the provider types supported by the TemplateType
func (t *TemplateTypeModel) GetProviderTypes() []TemplateProviderType {
	return t.ProviderTypes
}

// GetUpdateFieldNames returns the field names to be updated
func (t *TemplateTypeModel) GetUpdateFieldNames() []string {
	return t.UpdateFieldNames
}

// SetPrimaryID sets the primary ID of the TemplateType
func (t *TemplateTypeModel) SetPrimaryID(id string) {
	t.Name = id
}

// SetName sets the name of the TemplateType
func (t *TemplateTypeModel) SetName(name string) {
	t.Name = name
}

// SetFormats sets the formats supported by the TemplateType
func (t *TemplateTypeModel) SetFormats(formats []TemplateFormat) {
	t.Formats = formats
}

// SetEngine sets the engine for the TemplateType
func (t *TemplateTypeModel) SetEngine(engine TemplateEngine) {
	t.Engine = engine
}

// SetProviderTypes sets the provider types supported by the TemplateType
func (t *TemplateTypeModel) SetProviderTypes(providerTypes []TemplateProviderType) {
	t.ProviderTypes = providerTypes
}

// SetUpdateFieldNames sets the field names to be updated
func (t *TemplateTypeModel) SetUpdateFieldNames(fieldNames []string) {
	t.UpdateFieldNames = fieldNames
}

// TemplateTypeListItemModel is the element type in TemplateTypeListModel
type TemplateTypeListItemModel struct {
	Name          string                 `json:"name"`
	Formats       []TemplateFormat       `json:"formats,omitempty"`        // e.g., {'yaml', 'json'}
	Engine        TemplateEngine         `json:"engine"`                   // e.g., 'terraform'
	ProviderTypes []TemplateProviderType `json:"provider_types,omitempty"` // e.g., {'openstack', 'aws', `kubernetes`}
}

// TemplateTypeListModel is the template type list model
type TemplateTypeListModel struct {
	Session

	TemplateTypes []TemplateTypeListItemModel `json:"template_types"`
}

// GetTemplateTypes returns template types
func (t *TemplateTypeListModel) GetTemplateTypes() []TemplateType {
	templateTypes := make([]TemplateType, 0, len(t.TemplateTypes))

	for _, templateType := range t.TemplateTypes {
		templateTypeModel := TemplateTypeModel{
			Session:       t.Session,
			Name:          templateType.Name,
			Formats:       templateType.Formats,
			Engine:        templateType.Engine,
			ProviderTypes: templateType.ProviderTypes,
		}

		templateTypes = append(templateTypes, &templateTypeModel)
	}

	return templateTypes
}

// TemplateModel is the template model
type TemplateModel struct {
	Session

	ID          common.ID        `json:"id"`
	Owner       string           `json:"owner"`
	Name        string           `json:"name"`
	Description string           `json:"description"`
	Public      bool             `json:"public"`
	Source      TemplateSource   `json:"source"`
	Metadata    TemplateMetadata `json:"metadata"`
	CreatedAt   time.Time        `json:"created_at"`
	UpdatedAt   time.Time        `json:"updated_at"`

	UpdateFieldNames []string `json:"update_field_names,omitempty"`
	CredentialID     string   `json:"credential_id,omitempty"` // for import and sync
}

// NewTemplateID generates a new TemplateID
func NewTemplateID() common.ID {
	return common.NewID("template")
}

// GetPrimaryID returns the primary ID of the Template
func (t *TemplateModel) GetPrimaryID() common.ID {
	return t.ID
}

// GetID returns the ID of the Template
func (t *TemplateModel) GetID() common.ID {
	return t.ID
}

// GetOwner returns the owner of the Template
func (t *TemplateModel) GetOwner() string {
	return t.Owner
}

// GetName returns the name of the Template
func (t *TemplateModel) GetName() string {
	return t.Name
}

// GetDescription returns the description of the Template
func (t *TemplateModel) GetDescription() string {
	return t.Description
}

// IsPublic returns the accessibility of the Template, returns True if it is publically accessible
func (t *TemplateModel) IsPublic() bool {
	return t.Public
}

// GetSource returns the source of the Template
func (t *TemplateModel) GetSource() TemplateSource {
	return t.Source
}

// GetMetadata returns the metadata of the Template
func (t *TemplateModel) GetMetadata() TemplateMetadata {
	return t.Metadata
}

// GetCreatedAt returns the creation time of the Template
func (t TemplateModel) GetCreatedAt() time.Time {
	return t.CreatedAt
}

// GetUpdatedAt returns the modified time of the Template
func (t TemplateModel) GetUpdatedAt() time.Time {
	return t.UpdatedAt
}

// GetUpdateFieldNames returns the field names to be updated
func (t *TemplateModel) GetUpdateFieldNames() []string {
	return t.UpdateFieldNames
}

// GetCredentialID returns the credential id of the Template
func (t TemplateModel) GetCredentialID() string {
	return t.CredentialID
}

// SetPrimaryID sets the primary ID of the Template
func (t *TemplateModel) SetPrimaryID(id common.ID) {
	t.ID = id
}

// SetID sets the ID of the Template
func (t *TemplateModel) SetID(id common.ID) {
	t.ID = id
}

// SetName sets the name of the Template
func (t *TemplateModel) SetName(name string) {
	t.Name = name
}

// SetDescription sets the description of the Template
func (t *TemplateModel) SetDescription(description string) {
	t.Description = description
}

// SetPublic sets the accessibility of the Template
func (t *TemplateModel) SetPublic(isPublic bool) {
	t.Public = isPublic
}

// SetSource sets the source of the Template
func (t *TemplateModel) SetSource(source TemplateSource) {
	t.Source = source
}

// SetUpdateFieldNames sets the field names to be updated
func (t *TemplateModel) SetUpdateFieldNames(fieldNames []string) {
	t.UpdateFieldNames = fieldNames
}

// SetCredentialID sets the credential id of the Template
func (t *TemplateModel) SetCredentialID(credentialID string) {
	t.CredentialID = credentialID
}

// TemplateListItemModel is the element type in TemplateListModel
type TemplateListItemModel struct {
	ID          common.ID        `json:"id"`
	Owner       string           `json:"owner"`
	Name        string           `json:"name"`
	Description string           `json:"description"`
	Public      bool             `json:"public"`
	Source      TemplateSource   `json:"source"`
	Metadata    TemplateMetadata `json:"metadata"`
	CreatedAt   time.Time        `json:"created_at"`
	UpdatedAt   time.Time        `json:"updated_at"`
}

// TemplateListModel is the template list model
type TemplateListModel struct {
	Session

	Templates []TemplateListItemModel `json:"templates"`
}

// GetTemplates returns templates
func (t *TemplateListModel) GetTemplates() []Template {
	templates := make([]Template, 0, len(t.Templates))

	for _, template := range t.Templates {
		templateModel := TemplateModel{
			Session:     t.Session,
			ID:          template.ID,
			Owner:       template.Owner,
			Name:        template.Name,
			Description: template.Description,
			Public:      template.Public,
			Source:      template.Source,
			Metadata:    template.Metadata,
			CreatedAt:   template.CreatedAt,
			UpdatedAt:   template.UpdatedAt,
		}

		templates = append(templates, &templateModel)
	}

	return templates
}
