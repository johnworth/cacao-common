package http

import "time"

// KeyValue represents a key-value pair returned by the HTTP API
type KeyValue struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

// Parameters represents an object containing a list of key-value pairs.
type Parameters struct {
	Parameters []KeyValue `json:"parameters"`
}

// DeploymentResource represents a resource within a Deployment object returned by the HTTP API.
type DeploymentResource struct {
	ID                  string                 `json:"id"`
	Type                string                 `json:"type"`
	ProviderType        string                 `json:"provider_type"`
	Provider            string                 `json:"provider"`
	Attributes          map[string]interface{} `json:"attributes"`
	SensitiveAttributes interface{}            `json:"sensitive_attributes,omitempty"`
	AvailableActions    []string               `json:"available_actions"`
}

// DeploymentStateView represents a view of the state of a deployment.
type DeploymentStateView struct {
	Resources []DeploymentResource `json:"resources"`
}

// DeploymentBuild represents a build within a Deployment object returned by the HTTP API.
type DeploymentBuild struct {
	ID           string     `json:"id"`
	Owner        string     `json:"owner"`
	DeploymentID string     `json:"deployment_id"`
	Parameters   []KeyValue `json:"parameters"`
	Status       string     `json:"status"`
	Start        time.Time  `json:"start"`
	End          time.Time  `json:"end,omitempty"`
}

// DeploymentRun represents a run within a Deployment object returned by the HTTP API.
type DeploymentRun struct {
	ID               string              `json:"id"`
	Owner            string              `json:"owner"`
	DeploymentID     string              `json:"deployment_id"`
	BuildID          string              `json:"build_id"`
	Parameters       []KeyValue          `json:"parameters,omitempty"`
	CloudCredentials []string            `json:"cloud_credentials,omitempty"`
	GitCredentialID  string              `json:"git_credential_id,omitempty"`
	LastState        DeploymentStateView `json:"last_state,omitempty"`
	Status           string              `json:"status"`
	Start            time.Time           `json:"start"`
	End              time.Time           `json:"end,omitempty"`
}

// Deployment represents a deployment returned by the HTTP API.
type Deployment struct {
	ID                string          `json:"id"`
	Owner             string          `json:"owner"`
	WorkspaceID       string          `json:"workspace_id"`
	TemplateID        string          `json:"template_id"`
	PrimaryProviderID string          `json:"primary_provider_id"`
	CreatedAt         time.Time       `json:"created_at"`
	UpdatedAt         time.Time       `json:"updated_at"`
	Parameters        []KeyValue      `json:"parameters,omitempty"`
	CloudCredentials  []string        `json:"cloud_credentials,omitempty"`
	GitCredentialID   string          `json:"git_credential_id,omitempty"`
	CurrentBuild      DeploymentBuild `json:"current_build,omitempty"`
	CurrentRun        DeploymentRun   `json:"current_run,omitempty"`
}
