// Package common will be a deprecated package, referring to old nafigos content
package common

// SecretType associates a type with a Secret
type SecretType string

const (
	// GCRSecret is used for Google Container Registry
	GCRSecret SecretType = "gcr"
	// DockerhubSecret is used for Dockerhub
	DockerhubSecret SecretType = "dockerhub"
	// GitSecret is used for Github, GitLab, etc.
	GitSecret SecretType = "git"
)

// Secret ...
type Secret struct {
	Username string     `json:"username,omitempty"`
	Value    string     `json:"value,omitempty"`
	Type     SecretType `json:"type,omitempty"`
	ID       string     `json:"id,omitempty"`
}

// GetRedacted will return a new Secret with the Value set to "REDACTED"
func (s *Secret) GetRedacted() Secret {
	redactedSecret := *s
	redactedSecret.Value = "REDACTED"
	return redactedSecret
}

// User defines user attributes, primarily Secrets that are written to Vault.
// Note that the Password field is only used for creating the User
type User struct {
	Username string             `json:"username,omitempty"`
	Password string             `json:"password,omitempty,omitempty"`
	Secrets  map[string]Secret  `json:"secrets,omitempty"`
	Clusters map[string]Cluster `json:"clusters,omitempty"`
	IsAdmin  bool               `json:"is_admin"`
}

// HTTPUser contains the json data structure for between the api service and the client
type HTTPUser struct {
	Username     string            `json:"username"`
	FirstName    string            `json:"first_name"`
	LastName     string            `json:"last_name"`
	PrimaryEmail string            `json:"primary_email"`
	CreatedAt    string            `json:"created_at"`
	UpdatedAt    string            `json:"updated_at"`
	UpdatedBy    string            `json:"updated_by"`
	IsAdmin      bool              `json:"is_admin"`
	DisabledAt   string            `json:"disabled_at,omitempty"`
	Preferences  map[string]string `json:"preferences,omitempty"`
}

// GetRedacted will return a new User with sensitive fields redacted
func (u *User) GetRedacted() User {
	redactedUser := *u
	redactedSecrets := map[string]Secret{}
	redactedClusters := map[string]Cluster{}

	for k := range redactedUser.Secrets {
		s := redactedUser.Secrets[k]
		s.Value = "REDACTED"
		redactedSecrets[k] = s
	}
	for k := range redactedUser.Clusters {
		c := redactedUser.Clusters[k]
		c.Config = "REDACTED"
		redactedClusters[k] = c
	}

	redactedUser.Secrets = redactedSecrets
	redactedUser.Clusters = redactedClusters
	redactedUser.Password = "REDACTED"
	return redactedUser
}
