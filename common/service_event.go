package common

// HTTPStatus is used for responding to synchronous requests with an error that is JSON friendly
type HTTPStatus struct {
	Message string `json:"message,omitempty"` // Human-readable text message
	Code    int    `json:"code,omitempty"`    // HTTP status code
}

// ServiceRequestResult can be used for result of query requests and streaming requests
type ServiceRequestResult struct {
	Data   interface{} `json:"data"` // return object, can be nil
	Status HTTPStatus  `json:"status,omitempty"`
}
