package common

import (
	"strings"
	"unicode"

	"github.com/rs/xid"
)

// ID for entities, xid with a prefix. <prefix>-<xid>
type ID string

// NewID creates a new ID with a prefix.
// Prefix must be lowercase letters, No special characters or numbers may be used.
func NewID(prefix string) ID {
	return ID(prefix + "-" + xid.New().String())
}

// NewIDFromXID creates a new ID from a prefix and an existing xid.
func NewIDFromXID(prefix string, id xid.ID) ID {
	return ID(prefix + "-" + id.String())
}

// String ...
func (id ID) String() string {
	return string(id)
}

// Prefix returns the prefix
func (id ID) Prefix() string {
	return string(id[:strings.IndexRune(string(id), '-')])
}

// XID convert to XID
func (id ID) XID() (xid.ID, error) {
	return xid.FromString(string(id)[strings.IndexRune(string(id), '-')+1:])
}

// XIDMustParse convert to XID, panic if error
func (id ID) XIDMustParse() xid.ID {
	newXID, err := id.XID()
	if err != nil {
		panic(err)
	}
	return newXID
}

// Validate return true if id is validate
func (id ID) Validate() bool {
	if strings.Count(string(id), "-") != 1 {
		return false
	}
	if id.Prefix() == "" {
		return false
	}
	for _, rune := range id.Prefix() {
		if !unicode.IsLower(rune) {
			return false
		}
	}
	if _, err := id.XID(); err != nil {
		return false
	}
	return true
}
