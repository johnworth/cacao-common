package common

// QueryOp is a queue for a query operation
type QueryOp string

// EventType is a type to represent event operations
type EventType string

// EventID is the unique ID of event
type EventID string

// TransactionID is used to track transaction
type TransactionID string

// EventsSubject is the event channel that all cyverse events are posted
const EventsSubject = "cyverse.events"

// EventTypePrefix is the prefix for EventType
const EventTypePrefix = "org.cyverse.events."

// NatsQueryOpPrefix is the prefix for NatsQueryOp
const NatsQueryOpPrefix = "cyverse."

// EventTypeSet is a set of EventType
type EventTypeSet = map[EventType]struct{}
