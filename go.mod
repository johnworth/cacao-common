module gitlab.com/cyverse/cacao-common

go 1.16

require (
	github.com/cloudevents/sdk-go/v2 v2.3.1
	github.com/go-git/go-billy/v5 v5.3.1
	github.com/go-git/go-git/v5 v5.3.0
	github.com/google/uuid v1.1.1
	github.com/mitchellh/mapstructure v1.4.1
	github.com/nats-io/nats-streaming-server v0.22.0 // indirect
	github.com/nats-io/nats.go v1.11.0
	github.com/nats-io/stan.go v0.9.0
	github.com/rs/xid v1.2.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.6.1
	github.com/xeipuuv/gojsonschema v1.2.0
	go.mongodb.org/mongo-driver v1.5.2
	google.golang.org/protobuf v1.26.0 // indirect
)
