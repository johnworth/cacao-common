// Package messaging contains utility functions for dealing with a massaging service
package messaging

import (
	"fmt"
	"sync"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
)

// MockStanConnection mocks Stan connection
// implements StreamingEventService
type MockStanConnection struct {
	NatsConfig               *NatsConfig
	StanConfig               *StanConfig
	HandlerLock              sync.Mutex
	EventHandlers            map[common.EventType]StreamingEventHandler
	DefaultEventHandler      StreamingEventHandler
	CloudEventHandlers       map[common.EventType]StreamingCloudEventHandler
	DefaultCloudEventHandler StreamingCloudEventHandler
}

// CreateMockStanConnection creates MockStanConnection
func CreateMockStanConnection(natsConfig *NatsConfig, stanConfig *StanConfig, eventHandlerMappings []StreamingEventHandlerMapping) (*MockStanConnection, error) {
	mockStanConn := &MockStanConnection{
		NatsConfig:               natsConfig,
		StanConfig:               stanConfig,
		HandlerLock:              sync.Mutex{},
		EventHandlers:            map[common.EventType]StreamingEventHandler{},
		DefaultEventHandler:      nil,
		CloudEventHandlers:       map[common.EventType]StreamingCloudEventHandler{},
		DefaultCloudEventHandler: nil,
	}

	// lock handlers
	mockStanConn.HandlerLock.Lock()

	// Add handlers before subscribe
	for _, eventHandlerMapping := range eventHandlerMappings {
		if len(eventHandlerMapping.Subject) > 0 {
			if eventHandlerMapping.EventHandler != nil {
				mockStanConn.EventHandlers[common.EventType(eventHandlerMapping.Subject)] = eventHandlerMapping.EventHandler
			} else if eventHandlerMapping.CloudEventHandler != nil {
				mockStanConn.CloudEventHandlers[common.EventType(eventHandlerMapping.Subject)] = eventHandlerMapping.CloudEventHandler
			}
		} else {
			// default
			if eventHandlerMapping.EventHandler != nil {
				mockStanConn.DefaultEventHandler = eventHandlerMapping.EventHandler
			} else if eventHandlerMapping.CloudEventHandler != nil {
				mockStanConn.DefaultCloudEventHandler = eventHandlerMapping.CloudEventHandler
			}
		}
	}

	mockStanConn.HandlerLock.Unlock()

	return mockStanConn, nil
}

// Disconnect disconnects Stan connection
func (conn *MockStanConnection) Disconnect() error {
	conn.HandlerLock.Lock()
	conn.EventHandlers = map[common.EventType]StreamingEventHandler{}
	conn.DefaultEventHandler = nil
	conn.CloudEventHandlers = map[common.EventType]StreamingCloudEventHandler{}
	conn.DefaultCloudEventHandler = nil
	conn.HandlerLock.Unlock()
	return nil
}

// AddEventHandler adds a new event handler function to a specified subject
// eventHandler receives subject and JSON data of an event
func (conn *MockStanConnection) AddEventHandler(subject common.EventType, eventHandler StreamingEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockStanConnection.AddEventHandler",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.HandlerLock.Lock()
	conn.EventHandlers[common.EventType(subject)] = eventHandler
	conn.HandlerLock.Unlock()

	logger.Tracef("added an event handler for a subject %s", subject)
	return nil
}

// AddDefaultEventHandler adds a default event handler function. The handler is called when there is no matching handlers for the subject
// eventHandler receives subject and JSON data of an event
func (conn *MockStanConnection) AddDefaultEventHandler(eventHandler StreamingEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockStanConnection.AddDefaultEventHandler",
	})

	logger.Trace("adding a default event handler")

	conn.HandlerLock.Lock()
	conn.DefaultEventHandler = eventHandler
	conn.HandlerLock.Unlock()

	logger.Trace("added a default event handler")
	return nil
}

// AddCloudEventHandler adds a new event handler function to a specified subject
// eventHandler receives a cloudevent of an event
func (conn *MockStanConnection) AddCloudEventHandler(subject common.EventType, eventHandler StreamingCloudEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockStanConnection.SubscribeCloudEvent",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.HandlerLock.Lock()
	conn.CloudEventHandlers[common.EventType(subject)] = eventHandler
	conn.HandlerLock.Unlock()

	logger.Tracef("added an event handler for a subject %s", subject)
	return nil
}

// AddDefaultCloudEventHandler adds a default event handler function. The handler is called when there is no matching handlers for the subject
// eventHandler receives a cloudevent of an event
func (conn *MockStanConnection) AddDefaultCloudEventHandler(eventHandler StreamingCloudEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockStanConnection.AddDefaultCloudEventHandler",
	})

	logger.Trace("adding a default event handler")

	conn.HandlerLock.Lock()
	conn.DefaultCloudEventHandler = eventHandler
	conn.HandlerLock.Unlock()

	logger.Trace("added a default event handler")
	return nil
}

// Publish publishes Stan event
func (conn *MockStanConnection) Publish(subject common.EventType, data interface{}) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockStanConnection.Publish",
	})

	logger.Tracef("publishing a subject %s", subject)

	ce, err := CreateCloudEvent(data, string(subject), conn.NatsConfig.ClientID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return err
	}

	err = conn.publish(&ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish a subject %s", ce.Type())
		return err
	}

	logger.Tracef("published a subject %s", ce.Type())

	return nil
}

// PublishWithTransactionID publishes Stan event
func (conn *MockStanConnection) PublishWithTransactionID(subject common.EventType, data interface{}, transactionID common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockStanConnection.Publish",
	})

	logger.Tracef("publishing a subject %s", subject)

	ce, err := CreateCloudEventWithTransactionID(data, string(subject), conn.NatsConfig.ClientID, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return err
	}

	err = conn.publish(&ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish a subject %s", ce.Type())
		return err
	}

	logger.Tracef("published a subject %s", ce.Type())

	return nil
}

// PublishCloudEvent publishes Stan event
func (conn *MockStanConnection) PublishCloudEvent(ce *cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockStanConnection.PublishCloudEvent",
	})

	// we put a subject into Event.Type instead of Event.Subject
	// please refer 'CreateCloudEvent' function in cloudevent.go
	logger.Tracef("publishing a subject %s", ce.Type())

	err := conn.publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to request a subject %s", ce.Type())
		return err
	}

	logger.Tracef("published a subject %s", ce.Type())

	return nil
}

func (conn *MockStanConnection) publish(event *cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockStanConnection.publish",
	})

	conn.HandlerLock.Lock()
	defer conn.HandlerLock.Unlock()

	if handler, ok := conn.EventHandlers[common.EventType(event.Type())]; ok {
		// has the handler for the event
		transactionID := GetTransactionID(event)
		err := handler(common.EventType(event.Type()), transactionID, event.Data())
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return err
		}
		return nil
	} else if handler, ok := conn.CloudEventHandlers[common.EventType(event.Type())]; ok {
		// has the handler for the event
		err := handler(event)
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return err
		}
		return nil
	} else if conn.DefaultEventHandler != nil {
		// has the handler for the event
		transactionID := GetTransactionID(event)
		err := conn.DefaultEventHandler(common.EventType(event.Type()), transactionID, event.Data())
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return err
		}
		return nil
	} else if conn.DefaultCloudEventHandler != nil {
		// has the handler for the event
		err := conn.DefaultCloudEventHandler(event)
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return err
		}
		return nil
	} else {
		// ignore events
		return nil
	}
}
