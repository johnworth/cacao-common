// Package messaging -- this file contains utility functions for dealing with CloudEvents
package messaging

import (
	"encoding/json"
	"time"

	"gitlab.com/cyverse/cacao-common/common"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	nats "github.com/nats-io/nats.go"
	stan "github.com/nats-io/stan.go"
)

// CreateCloudEvent takes any object, eventType string, source string, and creates a resulting CloudEvent
// This utility provides the following conveniences:
// * uniformly assigns a new id of the format "cloudevent-" + xid
// * sets the time to UTC
// * generically marshals the data to json and appropriately assigns the cloudevent type
// * creates or sets a transaction id, which can later be used to pair requests to responses or corresponding events
func CreateCloudEvent(data interface{}, eventType string, source string) (cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	event.SetID(common.NewID("cloudevent").String())
	event.SetType(eventType)
	event.SetTime(time.Now().UTC())
	event.SetSource(source)

	// if data is json string, use it as it is
	// if not, jsonfy
	jsonfy := false
	if byteData, ok := data.([]byte); ok {
		if json.Valid(byteData) {
			event.SetData(cloudevents.ApplicationJSON, byteData)
			jsonfy = true
		}
	}

	if !jsonfy {
		b, err := json.Marshal(data)
		event.SetData(cloudevents.ApplicationJSON, b)
		return event, err
	}

	return event, nil
}

// CreateCloudEventWithTransactionID creates a CloudEvent with given transactionID, transactionID is optional
func CreateCloudEventWithTransactionID(data interface{}, eventType string, source string, transactionID common.TransactionID) (cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	event.SetID(common.NewID("cloudevent").String())
	event.SetType(eventType)
	event.SetTime(time.Now().UTC())
	event.SetSource(source)
	if len(transactionID) > 0 {
		event.SetExtension("TransactionID", string(transactionID))
	}

	// if data is json string, use it as it is
	// if not, jsonfy
	jsonfy := false
	if byteData, ok := data.([]byte); ok {
		if json.Valid(byteData) {
			event.SetData(cloudevents.ApplicationJSON, byteData)
			jsonfy = true
		}
	}

	if !jsonfy {
		b, err := json.Marshal(data)
		event.SetData(cloudevents.ApplicationJSON, b)
		return event, err
	}

	return event, nil
}

// NewTransactionID is a utility function that will generate a CACAO-specific transaction ID of the form tid-<xid>
// Replies and events generated from a specific request should include the transaction id for easier matching of requests
func NewTransactionID() common.TransactionID {
	return common.TransactionID(common.NewID("tid"))
}

// SetTransactionID is a utility function that writes TransactionID to CloudEvent quick
func SetTransactionID(ce *cloudevents.Event, transactionID common.TransactionID) {
	ce.SetExtension("TransactionID", string(transactionID))
}

// GetTransactionID is a utility function that reads TransactionID from CloudEvent quick
func GetTransactionID(ce *cloudevents.Event) common.TransactionID {
	transactionID, err := ce.Context.GetExtension("TransactionID")
	if err != nil {
		// return empty string when the event doesn't have transaction ID
		return ""
	}

	transactionIDString := transactionID.(string)
	return common.TransactionID(transactionIDString)
}

// ConvertNats converts NATS message to CloudEvents message
func ConvertNats(msg *nats.Msg) (cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	err := json.Unmarshal(msg.Data, &event)
	return event, err
}

// ConvertStan converts STAN message to CloudEvents message
func ConvertStan(msg *stan.Msg) (cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	err := json.Unmarshal(msg.Data, &event)
	return event, err
}
