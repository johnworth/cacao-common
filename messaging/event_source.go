package messaging

import (
	"errors"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/stan.go"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"sync"
)

// EventObservable is an interface that allow dynamically adding and removing event listener.
type EventObservable interface {
	AddListener(common.EventType, common.TransactionID, Listener) (ListenerID, error)
	// AddListenerMultiEventType register a listener to a set of common.EventType
	AddListenerMultiEventType([]common.EventType, common.TransactionID, Listener) (ListenerID, error)
	RemoveListenerByID(id ListenerID)
}

// Listener is callback registration for some EventType and/or TransactionID
type Listener struct {
	Callback ListenerCallback
	// if enabled, then listener will be removed after been called once.
	// default to false.
	ListenOnce bool
}

// ListenerCallback is the callback function for a Listener
type ListenerCallback func(ev common.EventType, ce cloudevents.Event)

// EventSource dispatch events to registered listeners.
// Listeners can be registered dynamically.
type EventSource struct {
	mappingLock sync.Mutex
	// mapping from eventType-TID to listener ID
	mapping map[listenerKey]ListenerID
	// mapping from listener ID to Callback
	listeners map[ListenerID]listenerEntry

	natsConf NatsConfig
	stanConf StanConfig

	connLock sync.Mutex
	sub      stan.Subscription
	sc       stan.Conn

	// for unit test.
	// if true then connection & subscription are not auto managed.
	// TODO use mock STAN connection for unit test, rather than disable auto connection management.
	unmanagedConn bool
}

// ListenerID is the ID of listener in EventSource
type ListenerID string

type listenerKey struct {
	// use "" for wildcard
	EventType common.EventType
	// transaction ID, use "" for wildcard
	Transaction common.TransactionID
}

type listenerEntry struct {
	// callback
	Callback ListenerCallback
	// keys that the listener registered to
	Keys []listenerKey
	// whether listener should be removed after being called once
	ListenOnce bool
}

// NewEventSource creates a new EventSource
func NewEventSource(natsConf NatsConfig, stanConf StanConfig) *EventSource {
	return &EventSource{
		mapping:   make(map[listenerKey]ListenerID),
		listeners: make(map[ListenerID]listenerEntry),
		natsConf:  natsConf,
		stanConf:  stanConf,
	}
}

// for unit test
func newEventSource() *EventSource {
	es := NewEventSource(NatsConfig{}, StanConfig{})
	es.unmanagedConn = true
	return es
}

// starts to listen to events if not already.
func (es *EventSource) start() error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "EventSource.start"})

	if es.unmanagedConn {
		return nil
	}

	logger.Trace("connecting and subscribing")

	es.connLock.Lock()
	defer es.connLock.Unlock()

	// skip connect/subscribe if has existing ones.
	if es.sc != nil || es.sub != nil {
		return nil
	}

	sc, err := stan.Connect(es.stanConf.ClusterID, es.natsConf.ClientID, stan.NatsURL(es.natsConf.URL))
	if err != nil {
		return err
	}
	es.sc = sc

	sub, err := sc.Subscribe(common.EventsSubject, es.handler)
	if err != nil {
		return err
	}
	es.sub = sub
	return nil
}

// unsubscribe and disconnect managed connection.
// set sub and sc to nil when unsubscribe/disconnect.
func (es *EventSource) close() {
	if es.unmanagedConn {
		return
	}

	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "EventSource.close"})
	logger.Trace("disconnecting and unsubscribing")

	es.connLock.Lock()
	defer es.connLock.Unlock()

	if es.sub != nil {
		err := es.sub.Unsubscribe()
		if err != nil {
			logger.Error(err)
		}
		es.sub = nil
	}
	if es.sc != nil {
		if err := es.sc.Close(); err != nil {
			logger.Error(err)
		}
		es.sc = nil
	}
}

// cleanup connection & subscription if no listener is registered
func (es *EventSource) cleanup() {
	es.mappingLock.Lock()
	length := len(es.mapping)
	es.mappingLock.Unlock()

	if length == 0 {
		es.close()
	}
}

func (es *EventSource) handler(m *stan.Msg) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "EventSource.handler"})
	ce, err := ConvertStan(m)
	if err != nil {
		logger.Error(err)
		return
	}
	if ce.Type() == "" {
		logger.WithField("event", m.Data).Error("event type missing")
		return
	}
	eventType := common.EventType(ce.Type())
	tid := GetTransactionID(&ce)

	logger = logger.WithFields(log.Fields{"evenType": ce.Type(), "transaction": tid})

	listeners := es.findListener(eventType, tid)
	if len(listeners) == 0 {
		logger.Trace("event ignored")
		return
	}
	for _, l := range listeners {
		go l(eventType, ce)
	}
	logger.WithField("listenerCount", len(listeners)).Trace("listeners called")

	es.cleanup()
}

// find listeners appropriate for an event, matched listeners are removed.
func (es *EventSource) findListener(eventType common.EventType, transactionID common.TransactionID) []ListenerCallback {
	es.mappingLock.Lock()
	defer es.mappingLock.Unlock()

	var matchedIDs [3]ListenerID
	var keys [3]listenerKey

	key := listenerKey{eventType, transactionID}
	listenerID, ok := es.mapping[key]
	if ok {
		matchedIDs[0] = listenerID
		keys[0] = key
	}

	if transactionID != "" {
		// check transaction wildcard
		key = listenerKey{eventType, ""}
		listenerID, ok = es.mapping[key]
		if ok {
			matchedIDs[1] = listenerID
			keys[1] = key
		}
	}

	// check event type wildcard
	key = listenerKey{"", transactionID}
	listenerID, ok = es.mapping[key]
	if ok {
		matchedIDs[2] = listenerID
		keys[2] = key
	}

	matched := make([]ListenerCallback, 0)
	for index, id := range matchedIDs {
		// if not matched, id should be empty, which should not have an entry in es.listeners
		entry, ok := es.listeners[id]
		if ok {
			// add listeners to matched
			matched = append(matched, entry.Callback)

			// remove only if listener is one-off
			if entry.ListenOnce {
				delete(es.mapping, keys[index])

				// remove all other keys registered by the listener
				for _, key := range entry.Keys {
					delete(es.mapping, key)
				}
				// remove listener
				delete(es.listeners, id)
			}
		}
	}
	return matched
}

// AddListener adds a listener for event.
// Use "" for eventType or transactionID as wildcard (cannot both be wildcard).
// Return a unique id of the listener, and connection-related error.
// This will automatically starts a STAN connection & subscription if not already.
func (es *EventSource) AddListener(eventType common.EventType, transactionID common.TransactionID, listener Listener) (ListenerID, error) {
	if eventType == "" && transactionID == "" {
		return "", errors.New("eventType and transactionID should not be empty at the same time")
	}

	if err := es.start(); err != nil {
		return "", err
	}

	key := listenerKey{eventType, transactionID}
	id := ListenerID(xid.New().String())

	// mappingLock
	es.mappingLock.Lock()
	defer es.mappingLock.Unlock()

	es.listeners[id] = listenerEntry{Callback: listener.Callback, Keys: []listenerKey{key}, ListenOnce: listener.ListenOnce}

	es.mapping[key] = id
	return id, nil
}

// AddListenerMultiEventType adds a listener for a set of event types.
// Use "" for eventType or transactionID as wildcard (cannot both be wildcard).
// Return a unique id of the listener, and connection-related error.
// If listener has set ListenOnce, it will be removed after being called once.
// This will automatically starts a STAN connection & subscription if not already.
func (es *EventSource) AddListenerMultiEventType(eventTypes []common.EventType, transactionID common.TransactionID, listener Listener) (ListenerID, error) {
	if transactionID == "" {
		for _, et := range eventTypes {
			if et == "" {
				return "", errors.New("eventType and transactionID should not be empty at the same time")
			}
		}
	}
	eventTypeSet := make(map[common.EventType]struct{})
	for _, et := range eventTypes {
		eventTypeSet[et] = struct{}{}
	}

	if err := es.start(); err != nil {
		return "", err
	}

	id := ListenerID(xid.New().String())
	entry := listenerEntry{Callback: listener.Callback, Keys: make([]listenerKey, 0), ListenOnce: listener.ListenOnce}

	// mappingLock
	es.mappingLock.Lock()
	defer es.mappingLock.Unlock()

	for et := range eventTypeSet {
		key := listenerKey{et, transactionID}
		entry.Keys = append(entry.Keys, key)
		es.mapping[key] = id
	}
	es.listeners[id] = entry
	return id, nil
}

// RemoveListenerByID removes a listener.
// This will disconnect & unsubscribe if the last listener is removed.
func (es *EventSource) RemoveListenerByID(id ListenerID) {
	es.mappingLock.Lock()
	defer es.mappingLock.Unlock()

	entry, ok := es.listeners[id]
	if !ok {
		return
	}
	for _, key := range entry.Keys {
		delete(es.mapping, key)
	}
	delete(es.listeners, id)

	// if last mapping is deleted, then close
	if len(es.mapping) == 0 {
		es.close()
	}
}
