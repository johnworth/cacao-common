package messaging

import (
	"context"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/stan.go"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestEventSourceImplements(t *testing.T) {
	assert.Implements(t, (*EventObservable)(nil), new(EventSource))
}

func createStanMsg(eventType common.EventType, tid common.TransactionID) stan.Msg {
	event, _ := CreateCloudEventWithTransactionID(map[string]string{}, string(eventType), "source", tid)
	payload, _ := event.MarshalJSON()
	var m stan.Msg
	m.Data = payload
	return m
}

func TestEventSource_handler(t *testing.T) {
	eventType := common.EventType("Created")
	eventTID := NewTransactionID()
	msg := createStanMsg(eventType, eventTID)

	es := newEventSource()

	var results = make([]int32, 3)
	ctx0, cancel0 := context.WithTimeout(context.Background(), time.Second*1)
	ctx1, cancel1 := context.WithTimeout(context.Background(), time.Second*1)
	ctx2, cancel2 := context.WithTimeout(context.Background(), time.Second*1)

	l0 := func(ev common.EventType, ce cloudevents.Event) {
		atomic.StoreInt32(&results[0], 1)
		cancel0()
	}
	l1 := func(ev common.EventType, ce cloudevents.Event) {
		atomic.StoreInt32(&results[1], 1)
		cancel1()
	}
	l2 := func(ev common.EventType, ce cloudevents.Event) {
		atomic.StoreInt32(&results[2], 1)
		cancel2()
	}

	_, err := es.AddListener(eventType, eventTID, Listener{l0, true})
	assert.NoError(t, err)
	assert.Equal(t, 1, len(es.mapping))
	assert.Equal(t, 1, len(es.listeners))
	_, err = es.AddListener("Different", eventTID, Listener{l1, true})
	assert.NoError(t, err)
	assert.Equal(t, 2, len(es.mapping))
	assert.Equal(t, 2, len(es.listeners))
	_, err = es.AddListener(eventType, NewTransactionID(), Listener{l2, true})
	assert.NoError(t, err)
	assert.Equal(t, 3, len(es.mapping))
	assert.Equal(t, 3, len(es.listeners))
	es.handler(&msg)
	assert.Equal(t, 2, len(es.mapping))
	assert.Equal(t, 2, len(es.listeners))

	<-ctx0.Done()
	<-ctx1.Done()
	<-ctx2.Done()
	assert.Equalf(t, int32(1), results[0], "listener 0 should be called")
	assert.Equalf(t, int32(0), results[1], "listener 1 should NOT be called")
	assert.Equalf(t, int32(0), results[2], "listener 2 should NOT be called")
}

// multiple msg with same eventType and transactionID, listener is one-off
func TestEventSource_handler_multipleMsgOneOff(t *testing.T) {
	eventType := common.EventType("Created")
	eventTID := NewTransactionID()
	msg := createStanMsg(eventType, eventTID)

	es := newEventSource()

	var mutexes [3]sync.Mutex
	var results = make([]int32, 3)
	ctx0, cancel0 := context.WithTimeout(context.Background(), time.Second)
	ctx1, cancel1 := context.WithTimeout(context.Background(), time.Second)
	ctx2, cancel2 := context.WithTimeout(context.Background(), time.Second)

	l0 := func(ev common.EventType, ce cloudevents.Event) {
		mutexes[0].Lock()
		defer mutexes[0].Unlock()
		results[0]++
		cancel0()
	}
	l1 := func(ev common.EventType, ce cloudevents.Event) {
		mutexes[1].Lock()
		defer mutexes[1].Unlock()
		results[1]++
		cancel1()
	}
	l2 := func(ev common.EventType, ce cloudevents.Event) {
		mutexes[2].Lock()
		defer mutexes[2].Unlock()
		results[2]++
		cancel2()
	}

	_, err := es.AddListener(eventType, eventTID, Listener{l0, true})
	assert.NoError(t, err)
	assert.Equal(t, 1, len(es.listeners))
	_, err = es.AddListener("Different", eventTID, Listener{l1, true})
	assert.NoError(t, err)
	assert.Equal(t, 2, len(es.listeners))
	_, err = es.AddListener(eventType, NewTransactionID(), Listener{l2, true})
	assert.NoError(t, err)
	assert.Equal(t, 3, len(es.listeners))

	// call handler on same msg for multiple times
	for i := 0; i < 10; i++ {
		es.handler(&msg)
		assert.Equal(t, 2, len(es.listeners))
	}

	<-ctx0.Done()
	<-ctx1.Done()
	<-ctx2.Done()
	assert.Equalf(t, int32(1), results[0], "listener 0 should be called once")
	assert.Equalf(t, int32(0), results[1], "listener 1 should NOT be called")
	assert.Equalf(t, int32(0), results[2], "listener 2 should NOT be called")
}

// multiple msg with same eventType and transactionID
func TestEventSource_handler_multipleMsg(t *testing.T) {
	eventType := common.EventType("Created")
	eventTID := NewTransactionID()
	msg := createStanMsg(eventType, eventTID)
	msgCount := 10

	es := newEventSource()

	var mutexes [3]sync.Mutex
	var results = make([]int32, 3)
	ctx0, cancel0 := context.WithTimeout(context.Background(), time.Second)
	ctx1, cancel1 := context.WithTimeout(context.Background(), time.Second)
	ctx2, cancel2 := context.WithTimeout(context.Background(), time.Second)

	l0 := func(ev common.EventType, ce cloudevents.Event) {
		mutexes[0].Lock()
		defer mutexes[0].Unlock()
		results[0]++
		cancel0()
	}
	l1 := func(ev common.EventType, ce cloudevents.Event) {
		mutexes[1].Lock()
		defer mutexes[1].Unlock()
		results[1]++
		cancel1()
	}
	l2 := func(ev common.EventType, ce cloudevents.Event) {
		mutexes[2].Lock()
		defer mutexes[2].Unlock()
		results[2]++
		cancel2()
	}

	_, err := es.AddListener(eventType, eventTID, Listener{l0, false})
	assert.NoError(t, err)
	assert.Equal(t, 1, len(es.mapping))
	assert.Equal(t, 1, len(es.listeners))
	_, err = es.AddListener("Different", eventTID, Listener{l1, false})
	assert.NoError(t, err)
	assert.Equal(t, 2, len(es.mapping))
	assert.Equal(t, 2, len(es.listeners))
	_, err = es.AddListener(eventType, NewTransactionID(), Listener{l2, false})
	assert.NoError(t, err)
	assert.Equal(t, 3, len(es.mapping))
	assert.Equal(t, 3, len(es.listeners))

	// call handler on same msg for multiple times
	for i := 0; i < msgCount; i++ {
		es.handler(&msg)
		assert.Equal(t, 3, len(es.mapping))
		assert.Equal(t, 3, len(es.listeners))
	}

	<-ctx0.Done()
	<-ctx1.Done()
	<-ctx2.Done()
	assert.Equalf(t, int32(msgCount), results[0], "listener 0 should be called %d times", msgCount)
	assert.Equalf(t, int32(0), results[1], "listener 1 should NOT be called")
	assert.Equalf(t, int32(0), results[2], "listener 2 should NOT be called")
}

func TestEventSource_handler_multiEventType(t *testing.T) {
	eventType := common.EventType("Created")
	eventTID := NewTransactionID()
	msg := createStanMsg(eventType, eventTID)

	es := newEventSource()

	var mutexes [3]sync.Mutex
	var results = make([]int32, 3)
	ctx0, cancel0 := context.WithTimeout(context.Background(), time.Second)
	ctx1, cancel1 := context.WithTimeout(context.Background(), time.Second)
	ctx2, cancel2 := context.WithTimeout(context.Background(), time.Second)

	l0 := func(ev common.EventType, ce cloudevents.Event) {
		mutexes[0].Lock()
		defer mutexes[0].Unlock()
		results[0]++
		cancel0()
	}
	l1 := func(ev common.EventType, ce cloudevents.Event) {
		mutexes[1].Lock()
		defer mutexes[1].Unlock()
		results[1]++
		cancel1()
	}
	l2 := func(ev common.EventType, ce cloudevents.Event) {
		mutexes[2].Lock()
		defer mutexes[2].Unlock()
		results[2]++
		cancel2()
	}

	_, err := es.AddListenerMultiEventType([]common.EventType{"Different", eventType}, eventTID, Listener{l0, true})
	assert.NoError(t, err)
	assert.Equal(t, 2, len(es.mapping))
	assert.Equal(t, 1, len(es.listeners))
	_, err = es.AddListener("", eventTID, Listener{l1, true})
	assert.NoError(t, err)
	assert.Equal(t, 3, len(es.mapping))
	assert.Equal(t, 2, len(es.listeners))
	_, err = es.AddListener(eventType, NewTransactionID(), Listener{l2, true})
	assert.NoError(t, err)
	assert.Equal(t, 4, len(es.mapping))
	assert.Equal(t, 3, len(es.listeners))
	for i := 0; i < 10; i++ {
		es.handler(&msg)
		assert.Equal(t, 1, len(es.mapping))
		assert.Equal(t, 1, len(es.listeners))
	}

	<-ctx0.Done()
	<-ctx1.Done()
	<-ctx2.Done()
	assert.Equalf(t, int32(1), results[0], "listener 0 should be called once")
	assert.Equalf(t, int32(1), results[1], "listener 1 should be called once")
	assert.Equalf(t, int32(0), results[2], "listener 2 should NOT be called")
}

func TestEventSource_handler_transactionWildcard(t *testing.T) {
	eventType := common.EventType("Created")
	eventTID := NewTransactionID()
	msg := createStanMsg(eventType, eventTID)

	es := newEventSource()

	var results = make([]int32, 3)
	ctx0, cancel0 := context.WithTimeout(context.Background(), time.Second)
	ctx1, cancel1 := context.WithTimeout(context.Background(), time.Second)
	ctx2, cancel2 := context.WithTimeout(context.Background(), time.Second)

	l0 := func(ev common.EventType, ce cloudevents.Event) {
		atomic.StoreInt32(&results[0], 1)
		cancel0()
	}
	l1 := func(ev common.EventType, ce cloudevents.Event) {
		atomic.StoreInt32(&results[1], 1)
		cancel1()
	}
	l2 := func(ev common.EventType, ce cloudevents.Event) {
		atomic.StoreInt32(&results[2], 1)
		cancel2()
	}

	_, err := es.AddListener(eventType, "", Listener{l0, true})
	assert.NoError(t, err)
	_, err = es.AddListener(eventType, eventTID, Listener{l1, true})
	assert.NoError(t, err)
	_, err = es.AddListener(eventType, NewTransactionID(), Listener{l2, true})
	assert.NoError(t, err)
	assert.Equal(t, 3, len(es.listeners))
	es.handler(&msg)
	assert.Equal(t, 1, len(es.listeners))

	<-ctx0.Done()
	<-ctx1.Done()
	<-ctx2.Done()
	assert.Equalf(t, int32(1), results[0], "listener 0 should be called")
	assert.Equalf(t, int32(1), results[1], "listener 1 should be called")
	assert.Equalf(t, int32(0), results[2], "listener 2 should NOT be called")
}

func TestEventSource_handler_eventTypeWildcard(t *testing.T) {
	eventType := common.EventType("Created")
	eventTID := NewTransactionID()
	msg := createStanMsg(eventType, eventTID)

	es := newEventSource()

	var results = make([]int32, 3)
	ctx0, cancel0 := context.WithTimeout(context.Background(), time.Second)
	ctx1, cancel1 := context.WithTimeout(context.Background(), time.Second)
	ctx2, cancel2 := context.WithTimeout(context.Background(), time.Second)

	l0 := func(ev common.EventType, ce cloudevents.Event) {
		atomic.StoreInt32(&results[0], 1)
		cancel0()
	}
	l1 := func(ev common.EventType, ce cloudevents.Event) {
		atomic.StoreInt32(&results[1], 1)
		cancel1()
	}
	l2 := func(ev common.EventType, ce cloudevents.Event) {
		atomic.StoreInt32(&results[2], 1)
		cancel2()
	}

	_, err := es.AddListener("", eventTID, Listener{l0, true})
	assert.NoError(t, err)
	_, err = es.AddListener(eventType, eventTID, Listener{l1, true})
	assert.NoError(t, err)
	_, err = es.AddListener(eventType, NewTransactionID(), Listener{l2, true})
	assert.NoError(t, err)
	assert.Equal(t, 3, len(es.listeners))
	es.handler(&msg)
	assert.Equal(t, 1, len(es.listeners))

	<-ctx0.Done()
	<-ctx1.Done()
	<-ctx2.Done()
	assert.Equalf(t, int32(1), results[0], "listener 0 should be called")
	assert.Equalf(t, int32(1), results[1], "listener 1 should be called")
	assert.Equalf(t, int32(0), results[2], "listener 2 should NOT be called")
}

func TestEventSource_handler_removeByID(t *testing.T) {
	eventType := common.EventType("Created")
	eventTID := NewTransactionID()
	es := newEventSource()
	l := func(ev common.EventType, ce cloudevents.Event) {}

	id1, _ := es.AddListener("", eventTID, Listener{l, true})
	assert.Equal(t, 1, len(es.mapping))
	assert.Equal(t, 1, len(es.listeners))
	_, err := es.AddListener(eventType, eventTID, Listener{l, true})
	assert.NoError(t, err)
	assert.Equal(t, 2, len(es.mapping))
	assert.Equal(t, 2, len(es.listeners))

	es.RemoveListenerByID(id1)
	assert.Equal(t, 1, len(es.mapping))
	assert.Equal(t, 1, len(es.listeners))
}
