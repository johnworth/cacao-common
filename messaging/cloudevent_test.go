package messaging

import (
	"encoding/json"
	"testing"

	nats "github.com/nats-io/nats.go"
	stan "github.com/nats-io/stan.go"
	"github.com/stretchr/testify/assert"
)

type testdata struct {
	field string
}

func TestCreateCloudEvent(t *testing.T) {
	cloudEvent, err := CreateCloudEvent(testdata{field: "request"}, "testType", "testSource")
	assert.NoError(t, err)
	assert.NotEmpty(t, cloudEvent)
	jsonBytes, err := cloudEvent.MarshalJSON()
	assert.NoError(t, err)
	assert.NotEmpty(t, jsonBytes)
}

func TestConvertNats(t *testing.T) {
	cloudEvent, err := CreateCloudEvent("request", "testType", "testSource")
	assert.NoError(t, err)
	assert.NotEmpty(t, cloudEvent)
	jsonBytes, err := cloudEvent.MarshalJSON()
	assert.NoError(t, err)
	assert.NotEmpty(t, jsonBytes)

	msg := nats.Msg{}
	msg.Data = jsonBytes

	cloudEventSecond, err := ConvertNats(&msg)
	assert.NoError(t, err)
	assert.NotEmpty(t, cloudEventSecond)

	var result string
	err = json.Unmarshal(cloudEventSecond.Data(), &result)
	assert.NoError(t, err)
	assert.Equal(t, "request", result)
	assert.Equal(t, "testType", cloudEventSecond.Type())
	assert.Equal(t, "testSource", cloudEventSecond.Source())
}

func TestConvertStan(t *testing.T) {
	cloudEvent, err := CreateCloudEvent("request", "testType", "testSource")
	assert.NoError(t, err)
	assert.NotEmpty(t, cloudEvent)
	jsonBytes, err := cloudEvent.MarshalJSON()
	assert.NoError(t, err)
	assert.NotEmpty(t, jsonBytes)

	msg := stan.Msg{}
	msg.Data = jsonBytes

	cloudEventSecond, err := ConvertStan(&msg)
	assert.NoError(t, err)
	assert.NotEmpty(t, cloudEventSecond)

	var result string
	err = json.Unmarshal(cloudEventSecond.Data(), &result)
	assert.NoError(t, err)
	assert.Equal(t, "request", result)
	assert.Equal(t, "testType", cloudEventSecond.Type())
	assert.Equal(t, "testSource", cloudEventSecond.Source())
}

func TestConvertStanWithTransactionID(t *testing.T) {
	transactionID := NewTransactionID()
	assert.NotEmpty(t, transactionID)
	cloudEvent, err := CreateCloudEventWithTransactionID("request", "testType", "testSource", transactionID)
	assert.NoError(t, err)
	assert.NotEmpty(t, cloudEvent)
	transactionID2 := GetTransactionID(&cloudEvent)
	assert.Equal(t, transactionID, transactionID2)

	jsonBytes, err := cloudEvent.MarshalJSON()
	assert.NoError(t, err)
	assert.NotEmpty(t, jsonBytes)

	msg := stan.Msg{}
	msg.Data = jsonBytes

	cloudEventSecond, err := ConvertStan(&msg)
	assert.NoError(t, err)
	assert.NotEmpty(t, cloudEventSecond)
	transactionID3 := GetTransactionID(&cloudEventSecond)
	assert.Equal(t, transactionID, transactionID3)

	var result string
	err = json.Unmarshal(cloudEventSecond.Data(), &result)
	assert.NoError(t, err)
	assert.Equal(t, "request", result)
	assert.Equal(t, "testType", cloudEventSecond.Type())
	assert.Equal(t, "testSource", cloudEventSecond.Source())
}
