// Package messaging contains utility functions for dealing with a massaging service
package messaging

import (
	"fmt"
	"sync"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	stan "github.com/nats-io/stan.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
)

const (
	// DefaultStanEventsTimeout is a default timeout
	DefaultStanEventsTimeout = 10 * time.Second // used to wait for event ops to complete, is very conservation
	// DefaultStanAckWaitTime is a default wait time for ack wait
	DefaultStanAckWaitTime = 60 * time.Second
)

// StanConfig stores additional configurations used by nats streaming, used along with NatsConfig DefaultNatsReconnectWait
type StanConfig struct {
	ClusterID     string `envconfig:"NATS_CLUSTER_ID" default:"cacao-cluster"`
	DurableName   string `envconfig:"NATS_DURABLE_NAME"`
	EventsTimeout int    `envconfig:"NATS_EVENTS_TIMEOUT" default:"-1"`
}

// StanConnection contains Stan connection info
type StanConnection struct {
	NatsConfig               *NatsConfig
	StanConfig               *StanConfig
	Connection               stan.Conn
	Subscription             stan.Subscription
	HandlerLock              sync.Mutex
	EventHandlers            map[common.EventType]StreamingEventHandler
	DefaultEventHandler      StreamingEventHandler
	CloudEventHandlers       map[common.EventType]StreamingCloudEventHandler
	DefaultCloudEventHandler StreamingCloudEventHandler
}

// ConnectStanForService connects to Stan
func ConnectStanForService(natsConfig *NatsConfig, stanConfig *StanConfig, eventHandlerMappings []StreamingEventHandlerMapping) (*StanConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "ConnectStanForService",
	})

	logger.Tracef("trying to connect to %s", natsConfig.URL)

	// Connect to Stan
	sc, err := stan.Connect(stanConfig.ClusterID, natsConfig.ClientID, stan.NatsURL(natsConfig.URL))
	if err != nil {
		logger.WithError(err).Errorf("failed to connect to %s", natsConfig.URL)
		return nil, err
	}

	stanConn := &StanConnection{
		NatsConfig:               natsConfig,
		StanConfig:               stanConfig,
		Connection:               sc,
		Subscription:             nil,
		HandlerLock:              sync.Mutex{},
		EventHandlers:            map[common.EventType]StreamingEventHandler{},
		DefaultEventHandler:      nil,
		CloudEventHandlers:       map[common.EventType]StreamingCloudEventHandler{},
		DefaultCloudEventHandler: nil,
	}

	// Add a handler
	handler := func(msg *stan.Msg) {
		err := msg.Ack()
		if err != nil {
			logger.Error(err)
		}

		ce, err := ConvertStan(msg)
		if err != nil {
			logger.Error(err)
		} else {
			// handle events
			err := stanConn.callEventHandler(&ce)
			if err != nil {
				logger.Error(err)
			}
		}
	}

	// lock handlers
	stanConn.HandlerLock.Lock()

	// Add handlers before subscribe
	for _, eventHandlerMapping := range eventHandlerMappings {
		if len(eventHandlerMapping.Subject) > 0 {
			if eventHandlerMapping.EventHandler != nil {
				stanConn.EventHandlers[common.EventType(eventHandlerMapping.Subject)] = eventHandlerMapping.EventHandler
			} else if eventHandlerMapping.CloudEventHandler != nil {
				stanConn.CloudEventHandlers[common.EventType(eventHandlerMapping.Subject)] = eventHandlerMapping.CloudEventHandler
			}
		} else {
			// default
			if eventHandlerMapping.EventHandler != nil {
				stanConn.DefaultEventHandler = eventHandlerMapping.EventHandler
			} else if eventHandlerMapping.CloudEventHandler != nil {
				stanConn.DefaultCloudEventHandler = eventHandlerMapping.CloudEventHandler
			}
		}
	}

	stanConn.HandlerLock.Unlock()

	// use QueueSubscribe API
	subscription, err := sc.QueueSubscribe(string(common.EventsSubject), natsConfig.QueueGroup, handler, stan.DurableName(stanConfig.DurableName), stan.SetManualAckMode(), stan.AckWait(DefaultStanAckWaitTime))
	if err != nil {
		logger.Error(err)
		sc.Close()
		return nil, err
	}

	stanConn.Subscription = subscription
	logger.Tracef("established a connection to %s", natsConfig.URL)

	return stanConn, nil
}

// ConnectStanForServiceClient connects to Stan for service clients who send events with no subscription
func ConnectStanForServiceClient(natsConfig *NatsConfig, stanConfig *StanConfig) (*StanConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "ConnectStanForServiceClient",
	})

	logger.Tracef("trying to connect to %s", natsConfig.URL)

	// Connect to Stan
	sc, err := stan.Connect(stanConfig.ClusterID, natsConfig.ClientID, stan.NatsURL(natsConfig.URL))
	if err != nil {
		logger.WithError(err).Errorf("failed to connect to %s", natsConfig.URL)
		return nil, err
	}

	stanConn := &StanConnection{
		NatsConfig:               natsConfig,
		StanConfig:               stanConfig,
		Connection:               sc,
		Subscription:             nil,
		HandlerLock:              sync.Mutex{},
		EventHandlers:            map[common.EventType]StreamingEventHandler{},
		DefaultEventHandler:      nil,
		CloudEventHandlers:       map[common.EventType]StreamingCloudEventHandler{},
		DefaultCloudEventHandler: nil,
	}

	logger.Tracef("established a connection to %s", natsConfig.URL)

	return stanConn, nil
}

// Disconnect disconnects Stan connection
func (conn *StanConnection) Disconnect() error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "StanConnection.Disconnect",
	})

	logger.Tracef("trying to disconnect from %s", conn.NatsConfig.URL)

	if conn.Connection == nil {
		err := fmt.Errorf("connection is not established")
		logger.Error(err)
		return err
	}

	if conn.Subscription != nil {
		conn.Subscription.Close()
	}

	if conn.Connection != nil {
		conn.Connection.Close()
	}

	logger.Tracef("disconnected from %s", conn.NatsConfig.URL)

	// clear
	conn.Connection = nil
	conn.Subscription = nil

	conn.HandlerLock.Lock()
	conn.EventHandlers = map[common.EventType]StreamingEventHandler{}
	conn.DefaultEventHandler = nil
	conn.CloudEventHandlers = map[common.EventType]StreamingCloudEventHandler{}
	conn.DefaultCloudEventHandler = nil
	conn.HandlerLock.Unlock()
	return nil
}

// AddEventHandler adds a new event handler function to a specified subject
// eventHandler receives subject and JSON data of an event
func (conn *StanConnection) AddEventHandler(subject common.EventType, eventHandler StreamingEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "StanConnection.AddEventHandler",
	})

	if conn.Subscription == nil {
		err := fmt.Errorf("the connection is not established for subscribing events")
		return err
	}

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.HandlerLock.Lock()
	conn.EventHandlers[common.EventType(subject)] = eventHandler
	conn.HandlerLock.Unlock()

	logger.Tracef("added an event handler for a subject %s", subject)

	return nil
}

// AddDefaultEventHandler adds a default event handler function. The handler is called when there is no matching handlers for the subject
// eventHandler receives subject and JSON data of an event
func (conn *StanConnection) AddDefaultEventHandler(eventHandler StreamingEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "StanConnection.AddDefaultEventHandler",
	})

	if conn.Subscription == nil {
		err := fmt.Errorf("the connection is not established for subscribing events")
		return err
	}

	logger.Trace("adding a default event handler")

	conn.HandlerLock.Lock()
	conn.DefaultEventHandler = eventHandler
	conn.HandlerLock.Unlock()

	logger.Trace("added a default event handler")
	return nil
}

// AddCloudEventHandler adds a new event handler function to a specified subject
// eventHandler receives a cloudevent of an event
func (conn *StanConnection) AddCloudEventHandler(subject common.EventType, eventHandler StreamingCloudEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "StanConnection.AddCloudEventHandler",
	})

	if conn.Subscription == nil {
		err := fmt.Errorf("the connection is not established for subscribing events")
		return err
	}

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.HandlerLock.Lock()
	conn.CloudEventHandlers[common.EventType(subject)] = eventHandler
	conn.HandlerLock.Unlock()

	logger.Tracef("added an event handler for a subject %s", subject)

	return nil
}

// AddDefaultCloudEventHandler adds a default event handler function. The handler is called when there is no matching handlers for the subject
// eventHandler receives a cloudevent of an event
func (conn *StanConnection) AddDefaultCloudEventHandler(eventHandler StreamingCloudEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "StanConnection.AddDefaultCloudEventHandler",
	})

	if conn.Subscription == nil {
		err := fmt.Errorf("the connection is not established for subscribing events")
		return err
	}

	logger.Trace("adding a default event handler")

	conn.HandlerLock.Lock()
	conn.DefaultCloudEventHandler = eventHandler
	conn.HandlerLock.Unlock()

	logger.Trace("added a default event handler")
	return nil
}

// Publish publishes Stan event
func (conn *StanConnection) Publish(subject common.EventType, data interface{}) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "StanConnection.Publish",
	})

	logger.Tracef("publishing a subject %s", subject)

	ce, err := CreateCloudEvent(data, string(subject), conn.NatsConfig.ClientID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return err
	}

	eventJSON, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Errorf("failed to marshal an event to JSON")
		return err
	}

	err = conn.Connection.Publish(common.EventsSubject, eventJSON)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish a subject %s", ce.Type())
		return err
	}

	logger.Tracef("published a subject %s", ce.Type())

	return nil
}

// PublishWithTransactionID publishes Stan event
// transactionID will be passed along to the CreateCloudEvent
func (conn *StanConnection) PublishWithTransactionID(subject common.EventType, data interface{}, transactionID common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "StanConnection.PublishWithTransactionID",
	})

	logger.Tracef("publishing a subject %s", subject)

	ce, err := CreateCloudEventWithTransactionID(data, string(subject), conn.NatsConfig.ClientID, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return err
	}

	eventJSON, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Errorf("failed to marshal an event to JSON")
		return err
	}

	err = conn.Connection.Publish(common.EventsSubject, eventJSON)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish a subject %s", ce.Type())
		return err
	}

	logger.Tracef("published a subject %s", ce.Type())

	return nil
}

// PublishCloudEvent publishes Stan event
func (conn *StanConnection) PublishCloudEvent(ce *cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "StanConnection.PublishCloudEvent",
	})

	// we put a subject into Event.Type instead of Event.Subject
	// please refer 'CreateCloudEvent' function in cloudevent.go
	logger.Tracef("publishing a subject %s", ce.Type())

	eventJSON, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Errorf("failed to marshal an event to JSON")
		return err
	}

	err = conn.Connection.Publish(common.EventsSubject, eventJSON)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish a subject %s", ce.Type())
		return err
	}

	logger.Tracef("published a subject %s", ce.Type())

	return nil
}

// calls a event handler for an event
func (conn *StanConnection) callEventHandler(event *cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "StanConnection.callEventHandler",
	})

	// we put a subject into Event.Type instead of Event.Subject
	// please refer 'CreateCloudEvent' function in cloudevent.go
	logger.Tracef("handling an event %s", event.Type())

	conn.HandlerLock.Lock()
	defer conn.HandlerLock.Unlock()

	if handler, ok := conn.EventHandlers[common.EventType(event.Type())]; ok {
		// has the handler for the event
		transactionID := GetTransactionID(event)
		err := handler(common.EventType(event.Type()), transactionID, event.Data())
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return err
		}
		return nil
	} else if handler, ok := conn.CloudEventHandlers[common.EventType(event.Type())]; ok {
		// has the handler for the event
		err := handler(event)
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return err
		}
		return nil
	} else if conn.DefaultEventHandler != nil {
		// has the handler for the event
		transactionID := GetTransactionID(event)
		err := conn.DefaultEventHandler(common.EventType(event.Type()), transactionID, event.Data())
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return err
		}
		return nil
	} else if conn.DefaultCloudEventHandler != nil {
		// has the handler for the event
		err := conn.DefaultCloudEventHandler(event)
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return err
		}
		return nil
	} else {
		// ignore events
		return nil
	}
}
