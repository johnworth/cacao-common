package format

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type testobject struct {
	ID    string `json:"id"`
	Owner string `json:"owner,omitempty"`
	Field string `json:"field,omitempty"`
}

type testobjectcomplex struct {
	ID        string                 `json:"id"`
	Owner     string                 `json:"owner,omitempty"`
	Testfloat float32                `json:"testfloat,omitempty"`
	Testobj   testobject             `json:"testobj,omitempty"`
	Testmap   map[string]interface{} `json:"testmap,omitempty"`
	Testarr   []string               `json:"testarr,omitempty"`
	Testarr2  []testobject           `json:"testarr2,omitempty"`
	Testtime  time.Time              `json:"testtime,omitempty"`
}

const (
	testobjectSchema string = `
	{
		"$schema": "http://json-schema.org/draft-07/schema#",
		"type": "object",
		"properties": {
			"id": {
				"type": "string",
				"minLength": 1,
				"maxLength": 50,
				"pattern": "^[A-Za-z0-9].*"
			},
			"owner": {
				"type": "string"
			},
			"field": {
				"type": "string"
			}
		},
		"required": ["id"],
		"additionalProperties": false
	}`

	testobjectcomplexSchema string = `
	{
		"$schema": "http://json-schema.org/draft-07/schema#",
	 	"definitions": {
			"testobject": ` + testobjectSchema + `
	 	},
	 	"type": "object",
	 	"properties": {
			"id": {
				"type": "string",
				"minLength": 1,
				"maxLength": 50,
				"pattern": "^[A-Za-z0-9].*"
			},
			"owner": {
				"type": "string"
			},
			"testfloat": {
				"type": "number"
			},
			"testobj": {
				"$ref": "#/definitions/testobject"
			},
			"testmap": {
				"type": "object",
				"additionalProperties": true		
			},
			"testarr": {
				"type": "array",
				"items": {
					"type": "string"
				}
			},
			"testarr2": {
				"type": "array",
				"items": {
					"$ref": "#/definitions/testobject"
				}
			}
		},
		"required": ["id"],
		"additionalProperties": true
	}`
)

func TestDecodeJSONStrictlySuccess(t *testing.T) {
	testJSONString := `{
		"id": "test134"
	}`
	var obj testobject
	err := DecodeJSONStrictly([]byte(testJSONString), &obj)
	assert.NoError(t, err)
}

func TestDecodeJSONStrictlyUnknownFieldError(t *testing.T) {
	testJSONString := `{
		"id": "test134",
		"unknown_field": 1234
	}`
	var obj testobject
	err := DecodeJSONStrictly([]byte(testJSONString), &obj)
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "unknown field")
}

func TestValidateJSONSchemaMandateFieldError(t *testing.T) {
	testJSONString := `{}`
	err := ValidateJSONSchema([]byte(testJSONString), []byte(testobjectSchema))
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "id is required")
}

func TestValidateJSONSchemaSuccess(t *testing.T) {
	testJSONString := `{
		"id": "test134"
	}`
	err := ValidateJSONSchema([]byte(testJSONString), []byte(testobjectSchema))
	assert.NoError(t, err)
}

func TestValidateJSONSchemaUnknownFieldError(t *testing.T) {
	testJSONString := `{
		"id": "test134",
		"unknown_field": 1234
	}`
	err := ValidateJSONSchema([]byte(testJSONString), []byte(testobjectSchema))
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "Additional property unknown_field is not allowed")
}

func TestValidateJSONSchemaComplexMandateFieldError(t *testing.T) {
	testJSONString := `{}`
	err := ValidateJSONSchema([]byte(testJSONString), []byte(testobjectcomplexSchema))
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "id is required")
}

func TestValidateJSONSchemaComplexSuccess(t *testing.T) {
	testJSONString := `{
		"id": "test134",
		"testfloat": 322.223,
		"testobj": {
			"id": "test134"
		},
		"testmap": {
			"abc": "def",
			"ccc": 1233
		},
		"testarr": ["abc", "def"],
		"testarr2": [
			{
				"id": "test134"
			}
		]
	}`
	err := ValidateJSONSchema([]byte(testJSONString), []byte(testobjectcomplexSchema))
	assert.NoError(t, err)
}
